<?php
namespace app\admin\model;

use think\Model;

class OperatorLogsModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_operator_logs';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getOperatorLogsByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('log_id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllOperatorLogs($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertOperatorLog($param){
        try{

            $result =  $this->validate('OperatorLogValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, '', '添加成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editOperatorLog($param){
        try{

            $result =  $this->validate('OperatorLogValidate')->save($param, ['log_id' => $param['log_id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, '', '编辑成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneOperatorLog($id){
        return $this->where('log_id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delOperatorLog($id){
        try{

            $this->where('log_id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据条件获取信息
     * @param $where
     */
    public function findOperatorLogByWhere($where){
        return $this->where($where)->select();
    }
}