<?php
namespace app\admin\controller;

use think\Controller;
use app\admin\model\OperatorLogsModel;
use app\admin\model\NodeModel;

class Base extends Controller
{
    public function _initialize()
    {
        if(empty(session('username'))){

            $loginUrl = url('login/index');
            if(request()->isAjax()){
                return msg(111, $loginUrl, '登录超时');
            }

            $this->redirect($loginUrl);
        }

        // 检测权限
        $control = lcfirst(request()->controller());
        $action = lcfirst(request()->action());

        if(empty(authCheck($control . '/' . $action))){
            if(request()->isAjax()){
                return msg(403, '', '您没有权限');
            }

            $this->error('403 您没有权限');
        }

        $this->assign([
            'username' => session('username'),
            'rolename' => session('role')
        ]);

        $this->assign([
            'singleUserEdit' => url('index/singleUserEdit', ['id' => session('id')])
        ]);

        if($action != 'index' && $action != 'indexpage' && $action != 'getdata' && $action != 'detail'){
            $this->recode_log();
        }

    }

    protected function recode_log(){
        $op_log = new OperatorLogsModel(); 
        $node_mdl = new NodeModel();
        $cur_ctl = request()->controller();
        $cur_act = request()->action();
        $cur_mtd = request()->method();
        $where['control_name'] = $cur_ctl;
        $where['action_name'] = $cur_act;
        $cur_node = $node_mdl->findNodeByWhere($where);
        $cur_ctl_act = (isset($cur_node['node_name'])) ? $cur_node['node_name']: '';

        $log = array(
            'op_id' => session('id'),
            'op_account' => session('username'),
            'op_name' => session('realname'),
            'op_bn' => session('user_bn'),
            'app' => request()->module(),
            'ctl' => $cur_ctl,
            'act' => $cur_act,
            'method' => $cur_mtd,
            'module' => request()->module(),
            'operate_type' => $cur_ctl_act,
            'param' => serialize(request()->param()),
            'dateline' => time(),
            
        );
        
        if(request()->isAjax() || request()->isPost()){
            $op_log->insertOperatorLog($log);
        }
    }
}