<?php

return [
    'url_route_on' => true,
    'trace' => [
        'type' => 'html', // 支持 socket trace file
    ],
    //各模块公用配置
    'extra_config_list' => ['database', 'route', 'validate'],
    //临时关闭日志写入
    'log' => [
        'type' => 'test',
    ],

    'app_debug' => true,

    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------
    'cache' => [
        // 驱动方式
        'type' => 'file',
        // 缓存保存目录
        'path' => CACHE_PATH,
        // 缓存前缀
        'prefix' => '',
        // 缓存有效期 0表示永久缓存
        'expire' => 0,
    ],

    //加密串
    'salt' => 'wZPb~yxvA!ir38&Z',

    //备份数据地址
    'back_path' => APP_PATH .'../back/',
    
    // 默认模块名
    'default_module'         => 'mobile',
    // 是否开启多语言
    'lang_switch_on'         => true,
    // 允许切换的语言列表 用逗号分隔
    'lang_list'        => ['zh-cn','en-us'],
    // 自动侦测语言 开启多语言功能后有效
    'lang_auto_detect' => true,
    // 默认语言切换变量 
    'var_language'     => 'l', 

    'http_exception_template' =>  [
        404 =>  APP_PATH.'404.html',
        403 =>  APP_PATH.'404.html',
    ],

];