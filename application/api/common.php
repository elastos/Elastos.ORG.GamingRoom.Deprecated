<?php

/**
 * 统一返回信息
 * @param $code
 * @param $data
 * @param $msge
 */
function msg($code, $msg){
    return compact('code', 'msg');
}

/**
 * 对象转换成数组
 * @param $obj
 */
function objToArray($obj){
    return json_decode(json_encode($obj), true);
}

// 随机生成6位唯一识别码
function randCodeCN($len = 6){
    $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    mt_srand((double)microtime()*1000000*getmypid());
    $code = '';
    while(strlen($code) < $len)
        $code .= substr($chars, (mt_rand()%strlen($chars)), 1);
    return $code;
}


