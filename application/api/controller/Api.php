<?php
/**
 * 授权基类，所有获取access_token以及验证access_token 异常都在此类中完成
 */
namespace app\api\controller;

use think\Controller;
use think\Request;
use think\Config;
use think\Exception;
use app\api\controller\Factory;
use app\api\controller\Send;
use think\response\Redirect;
use app\api\controller\UnauthorizedException;
use app\api\model\ApiLogsModel;
use app\api\model\MemberChargeModel;
use app\api\model\TransferModel;
use app\api\model\MembersModel;

header('Access-Control-Allow-Origin:*');     
header('Access-Control-Allow-Methods:*');  
header('Access-Control-Allow-Headers:*');

class Api extends Controller{	
	use Send;

	/**
     * 对应操作
     * @var array
     */
    public $methodToAction = [
        'get' => 'read',
        'post' => 'save',
        'put' => 'update',
        'delete' => 'delete',
        'patch' => 'patch',
        'head' => 'head',
        'options' => 'options',
    ];
    /**
     * 允许访问的请求类型
     * @var string
     */
    public $restMethodList = 'get|post|put|delete|patch|head|options';
    /**
     * 默认不验证
     * @var bool
     */
    public $apiAuth = false;

	protected $request;
	/**
     * 当前请求类型
     * @var string
     */
    protected $method;
    /**
     * 当前资源类型
     * @var string
     */
    protected $type;

    public static $app;
    /**
     * 返回的资源类的
     * @var string
     */
    protected $restTypeList = 'json';
    /**
     * REST允许输出的资源类型列表
     * @var array
     */
    protected $restOutputType = [ 
        'json' => 'application/json',
    ];

    /**
     * 客户端信息
     */
    protected $clientInfo;
	/**
	 * 控制器初始化操作
	 */
	public function _initialize(){	
    	$request = Request::instance();
    	$this->request = $request;
        $this->init();    //检查资源类型
        $this->clientInfo = $this->checkAuth();  //接口检查
    } 

    /**
     * 初始化方法
     * 检测请求类型，数据格式等操作
     */
    public function init(){
    	// 资源类型检测
        $request = Request::instance();
        $ext = $request->ext();
        if ('' == $ext) {
            // 自动检测资源类型
            $this->type = $request->type();
        } elseif (!preg_match('/\(' . $this->restTypeList . '\)$/i', $ext)) {
            // 资源类型非法 则用默认资源类型访问
            $this->type = $this->restDefaultType;
        } else {
            $this->type = $ext;
        }
        $this->setType();
        // 请求方式检测
        $method = strtolower($request->method());
        $this->method = $method;
        if (false === stripos($this->restMethodList, $method)) {

          return self::returnmsg(405,'Method Not Allowed',[],["Access-Control-Allow-Origin" => $this->restMethodList]);
        }
    }

    /**
     * 检测客户端是否有权限调用接口
     */
    public function checkAuth(){	
    	$baseAuth = Factory::getInstance(\app\api\controller\Oauth::class);
    	$clientInfo = $baseAuth->authenticate();
    	return $clientInfo;
    }
    /**
     * 空操作
     * 404
     * @return \think\Response|\think\response\Json|\think\response\Jsonp|\think\response\Xml
     */
    public function _empty(){
        return $this->sendSuccess([], 'empty method!', 200);
    }

    /**
     * API日志
     */
    protected function recode_log($api_param){
        $api_log = new ApiLogsModel();

        $log = array(
            'task_name' => $api_param['task_name'],
            'status' => $api_param['status'],
            'worker' => $api_param['worker'],
            'msg' => $api_param['msg'],
            'api_type' => $api_param['api_type'],
            'error_lv' => $api_param['error_lv'],
            'retry' => 0,
            'createtime' => time(),
            'last_modified' => time(),
        );
        if(isset($api_param['original_params'])) $log['original_params'] = serialize($api_param['original_params']);
        if(isset($api_param['params'])) $log['params'] = serialize($api_param['params']);
        if(isset($api_param['sync'])) $log['sync'] = $api_param['sync'];
        if(isset($api_param['log_type'])) $log['log_type'] = $api_param['log_type'];
        if(isset($api_param['memo'])) $log['memo'] = $api_param['memo'];
        if(isset($api_param['addon'])) $log['addon'] = $api_param['addon'];
        if(isset($api_param['source'])) $log['source'] = $api_param['source'];
        
        $where = array(); 
        $order = 'retry desc';
        if(isset($api_param['original_params']) && !empty($api_param['original_params'])){
            $where['original_params'] = serialize($api_param['original_params']);
        }
        if(isset($api_param['params']) && !empty($api_param['params'])){
            $where['params'] = serialize($api_param['params']);
        }
        $cur_log = $api_log->findApiLogsByWhere($where,$order);
        if(!empty($cur_log)){
            $log['retry'] = $cur_log[0]['retry']+1;
        }
        $api_log->insertApiLog($log);
    }

    /**
     * API异常日志处理
     */
    protected function recode_error_log($error_log, $msg){
        $error_log['error_lv'] = 'abnormity';
        $error_log['status'] = 'error';
        $error_log['msg'] = '异常：' . $msg;
        $this->recode_log($error_log);
    }

    /**
     * API验证日志处理
     */
    protected function recode_chk_log($chk_log, $msg){
        $chk_log['error_lv'] = 'normal';
        $chk_log['status'] = 'fail';
        $chk_log['msg'] = '验证：' . $msg;
        $this->recode_log($chk_log);
    }

    protected function get_log_data($task_name){
        return array(
            'task_name' => $task_name,
            'worker' => request()->action(),
            'api_type' => 'response',
            'error_lv' => 'normal'
        );
    }

    protected function curl_post_https($url,$data){ // 模拟提交数据函数
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
        // curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl,  CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Errno'.curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        return $tmpInfo; // 返回数据，json格式
    }
}