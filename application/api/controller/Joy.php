<?php
namespace app\api\controller;

use think\Controller;
use think\Request;
use app\api\controller\Api;
use think\Response;
use app\api\model\JoyItemModel;
use app\api\model\MembersModel;
use app\api\model\MembersJoyModel;
use app\api\model\MemberChargeModel;
use app\api\model\TransferModel;

class Joy extends Api{ 
    public $restMethodList = 'get|post|put';

    public function syncJoySubmit(){
        $cur_params = input('post.');
        if(isset($cur_params['hxjj'])) $cur_params = json_decode($cur_params['hxjj'],true);
        $log_data = $this->get_log_data('游戏提交');
        $log_data['cur_params'] = $cur_params;
        try{
            $chk_return = $this->checkJoyItem($cur_params);
            if(1 != $chk_return['code']){
                $log_data['status'] = 'fail';
                $this->recode_chk_log($log_data,$chk_return['msg']);
                return json_encode(array('status'=>'failed','error'=>$chk_return['code'], 'result'=>$cur_params['data'],'msg'=>$chk_return['msg']));
            }else{
                $joyItem_mdl = new JoyItemModel();
                $hash = $this->getHashMemo($cur_params['data']);
                $cur_params['data']['plaintext'] = $hash;
                $cur_params['data']['blockhash'] = hash('sha256', $hash);
                $cur_params['data']['create_time'] = time();
                $cur_params['data']['update_time'] = time();
                $log_data['source'] = $cur_params['source'];
                $log_data['return_cur_params'] = $cur_params;
                $return =$joyItem_mdl->insertJoyItem($cur_params['data']);
                if(!$return){
                    $log_data['status'] = 'fail';
                    $log_data['msg'] = '提交失败';
                    $this->recode_log($log_data);
                    return json_encode(array('status'=>'failed','error'=>-2,'result'=>$cur_params,'msg'=>'提交失败'));
                }else{
                    $log_data['status'] = 'success';
                    $log_data['msg'] = '提交成功';
                    $this->recode_log($log_data);

                    $joyItem = $joyItem_mdl->getOneJoyItem($return);
                    // $this->set_member_charge($joyItem['user_id']);
                    $url = $this->getWalletUrl($joyItem, $cur_params['data']);
                    return json_encode(array('status'=>'success','url' => $url));
                }
            }
        }catch (\Exception $e){
            $this->recode_error_log($log_data,$e->getMessage());
            return json_encode(array('status'=>'error','error'=>-2,'result'=>$cur_params,'msg'=>$e->getMessage()));
        }
    }

    private function checkJoyItem(&$params){
        if(!isset($params['source']) || empty($params['source'])){
            return msg(-3, '参数错误'); 
        }else if(!isset($params['data']) || empty($params['data'])){
            return msg(-3, '没有可操作的数据'); 
        }else{
            if(!isset($params['data']['uid'])){
                return msg(-1, '用户不存在');
            }
            $user_id = $params['data']['uid'];
            
            $members_mdl = new MembersModel();
            $hasUser = $members_mdl->getOneMember($user_id);
            if(empty($hasUser)){
                return msg(-1, '用户不存在');
            }
            
            if(1 != $hasUser['status']){
                return msg(-1, '账号已被禁用');
            }

            if(empty($hasUser['wallet_addr'])){
                return msg(-1, '用户未绑定钱包');
            }

            $params['data']['user_id'] = $user_id;
            unset($params['data']['uid']);
            $params['data']['use_time'] = $params['data']['time'];
            unset($params['data']['time']);
            $params['data']['start_time'] = $params['data']['starttime'];
            unset($params['data']['starttime']);
            $params['data']['end_time'] = $params['data']['endtime'];
            unset($params['data']['endtime']);
            $mj_mdl = new MembersJoyModel();
            $userInfo = $mj_mdl->getOneMemberJoyByUserId($user_id);
            $params['data']['team_id'] = $userInfo['team_id'];
        }
        
        return msg(1, ''); 
    }

    private function getWalletUrl($joyItem, $param){
        $walletUrl = config('txblock_url').'#?is_pay=true&account=10000';
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $address = config('address_url');
        $memo = 'chinajoy' . $joyItem['blockhash'];
        $callback_url = $http_type . $_SERVER['HTTP_HOST'] . '/api/txblock/syncBlock?id='.$joyItem['id'];
        $return_url = $http_type . $_SERVER['HTTP_HOST'] . '/mobile/joy/results.html?joy_id='.$joyItem['joy_id'].'&uid='.$joyItem['user_id'];

        $walletUrl .= '&address=' . $address . '&memo=' . $memo . '&callback_url=' . $callback_url . '&return_url=' . $return_url;
        return $walletUrl;
    }

    private function getHashMemo($params){
        $str = $params['user_id'] . $params['use_time'] . $params['score'] . randCodeCN();
        return $str;
    }

    /**
     * 手续费补贴
     * @规则:用户绑定钱包时会补贴一次手续费,此功能在绑定钱包时已执行
     */
    private function set_member_charge($user_id){
        $member_charge_mdl = new MemberChargeModel();
        //查看用户在member_charge表里有无记录 如果无记录 说明还未绑定ELA钱包地址 无法补贴手续费
        //取最新一条记录
        $newly = $member_charge_mdl->getNewlyOneByUserid($user_id);
        if($newly){
            /**
             * 1、查看时间
             * 2、如果时间在7小时之内 则不补贴 
             * 3、如果时间超过了7小时 则可以补贴 
             *  3-1、向transfer表插入记录 通知亦来云发送补贴
             *  3-2、然后在member_charge表新增记录，传入上一步的transfer_id
             */
            if(time() - $newly['create_time'] > 7*60*60){
                $transfer_mdl = new TransferModel();
                $members_mdl = new MembersModel();
                $wallet_addr = $members_mdl->getWalletAddrByUserid($user_id);
                $data = [];
                $data['wallet_addr'] = $wallet_addr;
                $data['amount'] = config('charge_amount');
                $transfer_id = $transfer_mdl->insertOneAndGetID($data);
                if($transfer_id > 0){
                    //添加member_charge表记录
                    $charge_data = [];
                    $charge_data['user_id'] = $user_id;
                    $charge_data['wallet_addr'] = $wallet_addr;
                    $charge_data['transfer_id'] = $transfer_id;
                    $charge_data['create_time'] = time();
                    $charge_data['update_time'] = time();
                    $member_charge_mdl->insertMemberCharge($charge_data);
                }
            }
        }
    }
}
