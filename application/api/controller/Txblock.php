<?php
namespace app\api\controller;

use think\Controller;
use think\Request;
use app\api\controller\Api;
use think\Response;
use app\api\model\JoyItemModel;

class Txblock extends Api{ 
    public $restMethodList = 'get|post|put';

    public function syncBlock(){
        $cur_params = input('get.');
        $log_data = $this->get_log_data('游戏区块上链');
        $log_data['cur_params'] = $cur_params;
        try{
            $chk_return = $this->checkJoyItem($cur_params);
            if(1 != $chk_return['code']){
                $log_data['status'] = 'fail';
                $this->recode_chk_log($log_data,$chk_return['msg']);
                return json_encode(array('status'=>'failed','error'=>$chk_return['code'], 'result'=>$cur_params,'msg'=>$chk_return['msg']));
            }else{
                $joyItem_mdl = new JoyItemModel();
                $cur_params = $this->getParams($cur_params);
                $cur_params['update_time'] = time();
                $log_data['return_cur_params'] = $cur_params;
                if($joyItem_mdl->editJoyItem($cur_params)){
                    $log_data['status'] = 'success';
                    $log_data['msg'] = '上链成功';
                    $this->recode_log($log_data);

                    return json_encode(array('status'=>'success','error'=>0,'result'=>$cur_params,'msg'=>'上链成功')); 
                }else{
                    $log_data['status'] = 'fail';
                    $log_data['msg'] = '上链失败';
                    $this->recode_log($log_data);

                    return json_encode(array('status'=>'fialed','error'=>-2,'result'=>$cur_params,'msg'=>'上链失败')); 
                }
            }
        }catch (\Exception $e){
            $this->recode_error_log($log_data,$e->getMessage());
            return json_encode(array('status'=>'error','error'=>-2,'result'=>$cur_params['data'],'msg'=>$e->getMessage()));
        }
    }

    private function checkJoyItem(&$params){
        if(!isset($params) || empty($params['txhash'])){
            return msg(-3, '没有可操作的数据'); 
        }else{
            if(!isset($params['id'])){
                return msg(-1, '参数错误');
            }

            $joyItem_mdl = new JoyItemModel();
            $joyItem = $joyItem_mdl->getOneJoyItem($params['id']);
            if(empty($joyItem)){
                return msg(-1, '数据不存在');
            }

            if(!empty($joyItem['txId'])){
                return msg(-1, '数据已上链');
            }
        }
        
        return msg(1, ''); 
    }

    private function getParams($data){
        $param = [];
        $param['id'] = $data['id'];
        $param['txId'] = $data['txhash'];
        return $param;
    }
}
