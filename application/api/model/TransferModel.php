<?php
namespace app\api\model;

use think\Model;

class TransferModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_transfer';

    /**
     * 插入一条记录
     */
    public function insertOneAndGetID($data){
        return $this->insertGetId($data);
    }
}