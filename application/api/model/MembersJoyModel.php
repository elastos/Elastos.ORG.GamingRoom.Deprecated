<?php
namespace app\api\model;

use think\Model;

class MembersJoyModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_members_joy';
    // 自动更新时间
    protected $autoWriteTimestamp = true;

    /**
     * 根据搜索条件获取用户列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getMembersJoyByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取所有的用户数量
     * @param $where
     */
    public function getAllMembersJoy($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertMemberJoy($param){
        try{
            $result =  $this->save($param);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editMemberJoy($param){
        try{
            $result =  $this->save($param, ['id' => $param['id']]);
            if(false === $result){
                return false;
            }else{

                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取角色信息
     * @param $id
     */
    public function getOneMemberJoy($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delMemberJoy($id){
        try{
            $this->where('id', $id)->delete();
            return true;
        }catch( PDOException $e){
            return false;
        }
    }

    /**
     * 根据user_id获取角色信息
     * @param $user_id
     */
    public function getOneMemberJoyByUserId($user_id){
        return $this->where('user_id', $user_id)->find();
    }
}