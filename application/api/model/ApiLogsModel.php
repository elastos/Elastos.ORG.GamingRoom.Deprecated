<?php
namespace app\api\model;

use think\Model;

class ApiLogsModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_api_logs';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getApiLogsByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('log_id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllApiLogs($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertApiLog($param){
        try{

            $result =  $this->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return false;
            }else{

                return true;
            }
        }catch(PDOException $e){

            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editApiLog($param){
        try{

            $result =  $this->save($param, ['log_id' => $param['log_id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return false;
            }else{

                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneApiLog($id){
        return $this->where('log_id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delApiLog($id){
        try{

            $this->where('log_id', $id)->delete();
            return true;

        }catch( PDOException $e){
            return false;
        }
    }

    /**
     * 根据条件获取信息
     * @param $where
     */
    public function findApiLogsByWhere($where,$order='log_id desc'){
        return $this->where($where)->order($order)->select();
    }

    /**
     * 根据条件获取信息
     * @param $where
     */
    public function getOneApiLogByWhere($where){
        return $this->where($where)->find();
    }
}