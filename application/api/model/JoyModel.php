<?php
namespace app\api\model;

use think\Model;

class JoyModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_joy';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getJoyByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllJoy($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertJoy($param){
        try{

            $result =  $this->save($param);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editJoy($param){
        try{
            $result =  $this->save($param, ['id' => $param['id']]);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneJoy($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delJoy($id){
        try{
            $this->where('id', $id)->delete();
            return true;
        }catch( PDOException $e){
            return false;
        }
    }

    /**
     * 根据名称获取信息
     * @param $name
     */
    public function findJoyByName($name){
        return $this->where('name', $name)->find();
    }
}