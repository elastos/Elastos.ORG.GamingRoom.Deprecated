<?php
namespace app\api\model;

use think\Model;

class JoyItemModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_joy_item';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getJoyItemsByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllJoyItems($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertJoyItem($param){
        try{
            $result =  $this->save($param);
            if(false === $result){
                return false;
            }else{
                return $this->getLastInsID();
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editJoyItem($param){
        try{
            $result =  $this->save($param, ['id' => $param['id']]);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneJoyItem($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delJoyItem($id){
        try{
            $this->where('id', $id)->delete();
            return true;
        }catch( PDOException $e){
            return false;
        }
    }

    /**
     * 根据条件获取信息
     * @param $where
     */
    public function getJoyItemByWhere($where){
        return $this->where($where)->select();
    }
}