<?php
namespace app\api\model;

use think\Model;

class MemberChargeModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_member_charge';

    // 根据user_id查询其最新一条记录
    public function getNewlyOneByUserid($user_id){
        return objToArray($this->where('user_id', $user_id)->order('create_time desc')->find());
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertMemberCharge($param){
        try{
            $result = $this->save($param);
            return false === $result ? false : true;
        }catch(PDOException $e){
            return false;
        }
    }
}