<?php
namespace app\api\model;

use think\Model;

class MembersModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_members';
    // 自动更新时间
    protected $autoWriteTimestamp = true;

    /**
     * 根据搜索条件获取用户列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getMembersByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')->where($where)->limit($offset, $limit)->order('user_id desc')->select();
    }

    /**
     * 根据搜索条件获取所有的用户数量
     * @param $where
     */
    public function getAllMembers($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertMember($param){
        try{
            $result =  $this->save($param);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editMember($param){
        try{
            $result =  $this->save($param, ['user_id' => $param['id']]);
            if(false === $result){
                return false;
            }else{

                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取角色信息
     * @param $id
     */
    public function getOneMember($id){
        return $this->where('user_id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delMember($id){
        try{
            $this->where('user_id', $id)->delete();
            return true;
        }catch( PDOException $e){
            return false;
        }
    }

    /**
     * 根据用户名获取信息
     * @param $name
     */
    public function findMemberByName($name){
        return $this->where('username', $name)->find();
    }

    /**
     * 根据手机获取信息
     * @param $mobile
     */
    public function findMemberByMobile($mobile){
        $mobile_check =  $this->where('mobile', $mobile)->find();
        if($mobile_check){
            return $mobile_check;
        }else{
            return $this->where('phone', $mobile)->find();
        }
    }

    public function editSingleMember($params){
        return $this->save($params, ['user_id' => $params['user_id']]);
    }

    public function editSingleMemberByUsername($username, $params){
        return $this->where('username', $username)->update($params);
    }

    /**
     * 根据用户id查询此用户邀请的所有用户名
     */
    public function getUsersByRefereeID($user_id){
        return objToArray($this->where('referee', $user_id)->order('create_time desc')->field('username, create_time')->select());
    }

    /**
     * 根据用户ident_code获取用户名
     */
    public function getUsernameByIdentcode($ident_code){
        return $this->where('ident_code', $ident_code)->value('username');
    }

    /**
     * 根据用户ident_code获取用户id
     */
    public function getUseridByIdentcode($ident_code){
        return $this->where('ident_code', $ident_code)->value('user_id');
    }

    /**
     * 根据用户id获取其密码
     */
    public function getPwdByID($user_id){
        return $this->where('user_id', $user_id)->value('password');
    }

    /**
     * 获取所有用户的id列表
     */
    public function getAllUserids(){
        return $this->column('user_id');
    }

    /**
     * 根据用户id获取其最大命数
     */
    public function getFateNumByID($user_id){
        return $this->where('user_id', $user_id)->value('fate_num');
    }

    /**
     * 根据用户id获取其ELA钱包地址
     */
    public function getWalletAddrByUserid($user_id){
        return $this->where('user_id', $user_id)->value('wallet_addr');
    }
}