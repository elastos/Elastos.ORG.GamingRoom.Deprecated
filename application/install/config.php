<?php
/**
 * 安装程序配置文件
 */
return array(
    //产品配置
    'install_product_name'   => 'elastos_', //产品名称
    'install_website_domain' => '', //官方网址
    'install_company_name'   => '通用后台', //公司名称
    'original_table_prefix'  => 'elastos_', //默认表前缀

    // 安装配置
    'install_table_total' => 56, // 安装时，需执行的sql语句数量
);
