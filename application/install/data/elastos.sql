/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : elastos

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-09-16 19:00:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for elastos_node
-- ----------------------------
DROP TABLE IF EXISTS `elastos_node`;
CREATE TABLE `elastos_node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_name` varchar(155) NOT NULL DEFAULT '' COMMENT '节点名称',
  `control_name` varchar(155) NOT NULL DEFAULT '' COMMENT '控制器名',
  `action_name` varchar(155) NOT NULL COMMENT '方法名',
  `is_menu` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否是菜单项 1不是 2是',
  `type_id` int(11) NOT NULL COMMENT '父级节点id',
  `style` varchar(155) DEFAULT '' COMMENT '菜单样式',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of elastos_node
-- ----------------------------
INSERT INTO `elastos_node` VALUES ('1', '用户管理', '#', '#', '2', '0', 'fa fa-users');
INSERT INTO `elastos_node` VALUES ('2', '管理员管理', 'user', 'index', '2', '1', '');
INSERT INTO `elastos_node` VALUES ('3', '添加管理员', 'user', 'useradd', '1', '2', '');
INSERT INTO `elastos_node` VALUES ('4', '编辑管理员', 'user', 'useredit', '1', '2', '');
INSERT INTO `elastos_node` VALUES ('5', '删除管理员', 'user', 'userdel', '1', '2', '');
INSERT INTO `elastos_node` VALUES ('6', '角色管理', 'role', 'index', '2', '1', '');
INSERT INTO `elastos_node` VALUES ('7', '添加角色', 'role', 'roleadd', '1', '6', '');
INSERT INTO `elastos_node` VALUES ('8', '编辑角色', 'role', 'roleedit', '1', '6', '');
INSERT INTO `elastos_node` VALUES ('9', '删除角色', 'role', 'roledel', '1', '6', '');
INSERT INTO `elastos_node` VALUES ('10', '分配权限', 'role', 'giveaccess', '1', '6', '');
INSERT INTO `elastos_node` VALUES ('15', '节点管理', 'node', 'index', '2', '1', '');
INSERT INTO `elastos_node` VALUES ('16', '添加节点', 'node', 'nodeadd', '1', '15', '');
INSERT INTO `elastos_node` VALUES ('17', '编辑节点', 'node', 'nodeedit', '1', '15', '');
INSERT INTO `elastos_node` VALUES ('18', '删除节点', 'node', 'nodedel', '1', '15', '');

-- ----------------------------
-- Table structure for elastos_role
-- ----------------------------
DROP TABLE IF EXISTS `elastos_role`;
CREATE TABLE `elastos_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_name` varchar(155) NOT NULL COMMENT '角色名称',
  `rule` varchar(255) DEFAULT '' COMMENT '权限节点数据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of elastos_role
-- ----------------------------
INSERT INTO `elastos_role` VALUES ('1', '超级管理员', '*');
INSERT INTO `elastos_role` VALUES ('2', '系统维护员', '1,2,3,4,5,6,7,8,9,10');

-- ----------------------------
-- Table structure for elastos_user
-- ----------------------------
DROP TABLE IF EXISTS `elastos_user`;
CREATE TABLE `elastos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '密码',
  `login_times` int(11) NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `last_login_ip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `last_login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `real_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '真实姓名',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `role_id` int(11) NOT NULL DEFAULT '1' COMMENT '用户角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of elastos_user
-- ----------------------------
INSERT INTO `elastos_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '41', '127.0.0.1', '1505559479', 'admin', '1', '1');

-- ----------------------------
-- Table structure for elastos_operator_logs
-- ----------------------------
DROP TABLE IF EXISTS `elastos_operator_logs`;
CREATE TABLE `elastos_operator_logs` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `op_id` int(10) DEFAULT NULL COMMENT '操作人ID',
  `op_account` varchar(50) DEFAULT NULL COMMENT '操作员账号',
  `op_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `op_bn` varchar(50) DEFAULT NULL COMMENT '操作人编号',
  `app` varchar(50) NOT NULL COMMENT '程序目录',
  `ctl` varchar(50) NOT NULL COMMENT '控制器',
  `act` varchar(50) DEFAULT NULL COMMENT '动作',
  `method` enum('post','get') NOT NULL DEFAULT 'post' COMMENT '提交方法',
  `module` varchar(255) NOT NULL COMMENT '日志模块',
  `operate_type` varchar(255) NOT NULL COMMENT '操作类型',
  `param` varchar(255) DEFAULT NULL COMMENT '参数',
  `dateline` int(11) unsigned NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE IF EXISTS `elastos_members_joy`;
CREATE TABLE `elastos_members_joy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户openid',
  `team_id` int(3) DEFAULT '0' COMMENT '团队ID',
  `medal_num` int(5) DEFAULT '0' COMMENT '奖牌数',
  PRIMARY KEY (`id`),
  KEY `index_user_id` (`user_id`),
  KEY `index_team_id` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_teams
-- ----------------------------
DROP TABLE IF EXISTS `elastos_teams`;
CREATE TABLE `elastos_teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '团队名(英文)',
  `logo` varchar(255) DEFAULT NULL COMMENT '团队LOGO',
  `is_valid` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：0无效，1有效',
  `total_eco_currency` int(10) NOT NULL DEFAULT '0' COMMENT '生态币总额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of elastos_teams
-- ----------------------------
INSERT INTO `elastos_teams` VALUES ('1', 'BIT.GAME', '/upload/teams/BITGAME.png', '1', '0');
INSERT INTO `elastos_teams` VALUES ('2', 'ViewChain', '/upload/teams/ViewChain.png', '1', '0');
INSERT INTO `elastos_teams` VALUES ('3', 'ioeX', '/upload/teams/ioeX.png', '1', '0');
INSERT INTO `elastos_teams` VALUES ('4', 'HASHWORLD', '/upload/teams/HASHWORLD.png', '1', '0');

-- ----------------------------
-- Table structure for elastos_joy_item
-- ----------------------------
DROP TABLE IF EXISTS `elastos_joy_item`;
CREATE TABLE `elastos_joy_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `team_id` int(11) NOT NULL DEFAULT '0' COMMENT '团队ID',
  `joy_id` int(11) NOT NULL DEFAULT '0' COMMENT '游戏ID',
  `use_time` int(11) NOT NULL DEFAULT '0' COMMENT '花费时间',
  `score` int(8) NOT NULL DEFAULT '0' COMMENT '分数',
  `start_time` int(11) NOT NULL DEFAULT '0' COMMENT '游戏开始时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '游戏结束时间',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '提交时间',
  `plaintext` varchar(200) DEFAULT NULL COMMENT '区块hash明文',
  `blockhash` varchar(64) DEFAULT NULL COMMENT '区块hash',
  `status` int(3) NOT NULL DEFAULT '0' COMMENT '状态：0  初始状态 1 已中奖',
  `txId` varchar(100) DEFAULT '',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_user_id` (`user_id`),
  KEY `index_joy_id` (`joy_id`),
  KEY `index_txId` (`txId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_member_team
-- ----------------------------
DROP TABLE IF EXISTS `elastos_member_team`;
CREATE TABLE `elastos_member_team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `team_id` int(11) NOT NULL DEFAULT '0' COMMENT '团队ID',
  `medal_num` int(5) NOT NULL DEFAULT '0' COMMENT '奖牌数',
  `wallet_addr` varchar(255) DEFAULT NULL COMMENT '生态团队钱包地址',
  PRIMARY KEY (`id`),
  KEY `index_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_joy
-- ----------------------------
DROP TABLE IF EXISTS `elastos_joy`;
CREATE TABLE `elastos_joy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '游戏ID',
  `name` varchar(100) NOT NULL COMMENT '游戏名称',
  `is_award` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有奖：0无奖；1有奖',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of elastos_joy
-- ----------------------------
INSERT INTO `elastos_joy` VALUES ('1', '星际迷航', '1');
INSERT INTO `elastos_joy` VALUES ('2', '火柴人', '0');
INSERT INTO `elastos_joy` VALUES ('3', '火线进击', '0');


-- ----------------------------
-- Table structure for elastos_team_switch_log
-- ----------------------------
DROP TABLE IF EXISTS `elastos_team_switch_log`;
CREATE TABLE `elastos_team_switch_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `exit_team_id` int(11) NOT NULL DEFAULT '0' COMMENT '退出团队ID',
  `join_team_id` int(11) NOT NULL DEFAULT '0' COMMENT '加入团队ID',
  `exit_time` int(11) NOT NULL DEFAULT '0' COMMENT '退出时间',
  `join_time` int(11) NOT NULL DEFAULT '0' COMMENT '加入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_msg
-- ----------------------------
DROP TABLE IF EXISTS `elastos_msg`;
CREATE TABLE `elastos_msg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '消息类型：0其他消息；1出块消息；2排名奖励；3手续费记录',
  `content` longtext COMMENT '消息内容',
  `receive_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '接受消息人ID',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `index_type` (`type`),
  KEY `index_receive_user_id` (`receive_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_api_logs
-- ----------------------------
DROP TABLE IF EXISTS `elastos_api_logs`;
CREATE TABLE `elastos_api_logs` (
  `log_id` int(32) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `task_name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `status` enum('running','success','fail','error') NOT NULL DEFAULT 'running' COMMENT '状态 running:运行中;success:成功;fail:失败;error:出错;',
  `worker` varchar(200) NOT NULL COMMENT 'api方法名',
  `params` longtext COMMENT '接口参数',
  `return_params` longtext COMMENT '返回参数',
  `sync` enum('true','false') DEFAULT NULL COMMENT '同异步类型 true:同步;false:异步;',
  `msg` text COMMENT '信息',
  `log_type` varchar(32) DEFAULT NULL COMMENT '日志类型',
  `api_type` enum('response','request') NOT NULL DEFAULT 'request' COMMENT '同步类型 response:响应;request:请求;',
  `error_lv` enum('normal','system','application','warning','abnormity') NOT NULL DEFAULT 'normal' COMMENT '错误级别 normal:正常;system:系统级;application:应用级;warning:警告;abnormity:异常;',
  `memo` text COMMENT '备注',
  `retry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '重试次数',
  `addon` text COMMENT '附加参数',
  `createtime` int(10) unsigned DEFAULT NULL COMMENT '发起时间',
  `last_modified` int(10) unsigned DEFAULT NULL COMMENT '最后重试时间',
  `source` enum('joy','other') DEFAULT 'joy' COMMENT '数据来源',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_member_charge
-- ----------------------------
DROP TABLE IF EXISTS `elastos_member_charge`;
CREATE TABLE `elastos_member_charge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `wallet_addr` varchar(255) DEFAULT NULL COMMENT '钱包地址',
  `is_issue` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否发放：0未发放；1已发放',
  `is_notice` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否通知：0未通知；1已通知',
  `transfer_id` int(11) NOT NULL DEFAULT '0' COMMENT '此次补贴在transfer表里的id',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_daily_top
-- ----------------------------
DROP TABLE IF EXISTS `elastos_daily_top`;
CREATE TABLE `elastos_daily_top` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `team_id` int(11) NOT NULL DEFAULT '0' COMMENT '生态企业ID',
  `joy_id` int(11) NOT NULL DEFAULT '0' COMMENT '游戏ID',
  `is_issue` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否发放：0未发放；1已发放',
  `is_notice` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否通知：0未通知；1已通知',
  `cur_time` int(11) NOT NULL COMMENT '计算日期',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `txid` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_id` (`user_id`),
  KEY `index_joy_id` (`joy_id`),
  KEY `index_cur_time` (`cur_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for elastos_height_blocks
-- ----------------------------
DROP TABLE IF EXISTS `elastos_height_blocks`;
CREATE TABLE `elastos_height_blocks` (
  `height` int(11) NOT NULL COMMENT '区块高度',
  `timestamp` int(11) NOT NULL COMMENT '区块时间戳',
  `transaction_num` int(11) NOT NULL DEFAULT '0' COMMENT '交易记录数目',
  `rewarded` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否已发送奖牌',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`height`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
