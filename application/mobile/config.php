<?php

return [
    // 模板参数替换
    'view_replace_str'       => array(
        '__CSS__'    => '/static/mobile/css',
        '__JS__'     => '/static/mobile/js',
        '__IMG__' => '/static/mobile/images',
    ),

    // 微信相关配置
    'wechat' => [
        'AppID' => 'wx56c754784daaca47',
        'AppSecret' => '',

        //以snsapi_base方式获取code的接口url
        'get_code_snsapi_base_url' => 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=%s#wechat_redirect',
        //以snsapi_userinfo方式获取code的接口url
        'get_code_snsapi_userinfo_url' => 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo&state=%s#wechat_redirect',
        //获取到code后回调的本地方法
        'get_code_redirect_url' => 'http://kuaidian.bizsov.com/mobile/wechat/get_code_redirect',
        //获取网页授权access_token的接口url
        'get_oauth2_access_token_url' => 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code',
        //获取用户信息接口url
        'get_userinfo_url' => 'https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=%s',
        //获取基础access_token的接口url
        'get_base_access_token_url' => 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s',
    ],

    //游戏url
    'joy_url' => [
        '1' => '/games/xjmh/index.html',//星际迷航
        '2' => '/games/stickrunning/index.html',//火柴人
        '3' => '/games/hxjj/index.html',//火线出击
    ],

    //创建钱包url
    'create_wallet_url' => [
        'ELA' => 'https://wallet.elastos.org',
        'BIT.GAME' => 'BIT.GAME_url',
        'ViewChain' => 'ViewChain_url',
        'ioeX' => 'ioeX_url',
        'HASHWORLD' => 'HASHWORLD_url',
    ],

    //消息类型
    'msg_types' => [
        0 => [
            'type' => -1,
            'type_name' => '全部消息'
        ],
        1 => [
            'type' => 1,
            'type_name' => '出块奖励'
        ],
        2 => [
            'type' => 2,
            'type_name' => '排名奖励'
        ],
        // 3 => [
        //     'type' => 3,
        //     'type_name' => '手续费补贴'
        // ],
        4 => [
            'type' => 0,
            'type_name' => '其它消息'
        ],
    ],

    //消息内容
    'msg_content' => [
        0 => '其它消息内容模板',
        1 => '您在上个块中提交的成绩被幸运选中了！恭喜您获得奖牌1个！提交的交易越多，被选中的概率越大哦！奖牌越多，瓜分的ELA越多哦！',
        2 => '恭喜您昨日在全体排行榜中排名前十！荣获奖牌5个！继续加油，奖牌越多，瓜分的ELA奖金越多哦！',
        3 => '您已收到奖励 0.0001 ELA，可用于支付在 “Elastos 欢乐竞猜”  和 “Elastos 游戏中心” 中参与游戏后提交成绩上链的手续费。数据上链越多，赢取的奖励越多，快来参与吧！',
    ],

    //获奖状态
    'ranking_status' => [
        0 => '否',
        1 => '是',
    ],

    //手续费补贴金额
    'charge_amount' => 0.0001,

    //获奖状态
    'header_title' => [
        'index' => [
            'index' => '首页',
        ],
        'joy' => [
            'index' => '游戏大厅',
            'ranking_list' => '游戏排行榜',
            'results' => '提交结果',
            'rules' => '游戏奖励规则',
            '404' => '404',
        ],
        'member' => [
            'index' => '个人中心',
            'personal_game_score' => '个人成绩',
            'personal_message' => '我的消息',
            'set_wallet_addr' => '设置钱包地址',
            'switch_team' => '切换企业冠名',
        ],
        'block' => [
            // 'index' => 'ChinaJoy奖赏榜',
            'index' => '排行榜',
            'block_records' => '区块抽奖',
            'block_record_detail' => '区块明细',
            'ranking_list' => '全体排行榜',
            'ranking_detail' => '获奖明细',
            'ranking_teams' => '生态排行榜',
        ]
    ],

];
