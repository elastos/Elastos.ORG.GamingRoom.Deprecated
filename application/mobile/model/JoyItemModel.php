<?php
namespace app\mobile\model;

use think\Model;

class JoyItemModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_joy_item';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getJoyItemsByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllJoyItems($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertJoyItem($param){
        try{

            $result =  $this->validate('JoyItemValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '添加成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editJoyItem($param){
        try{

            $result =  $this->validate('JoyItemValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '编辑成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneJoyItem($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delJoyItem($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据条件获取信息
     * @param $where
     */
    public function getJoyItemByWhere($where, $limit, $offset){
        $joyItems = $this->alias('ji')->field('ji.*, m.nickname, m.headimgurl, m.openid,mj.team_id, mj.medal_num as ela_medal_num, t.name as teamname, t.logo, t.total_eco_currency')
            ->join('elastos_members m', 'ji.user_id=m.user_id')
            ->join('elastos_members_joy mj', 'ji.user_id=mj.user_id')
            ->join('elastos_teams t', 'ji.team_id=t.id','LEFT')
            ->where($where)->limit($offset, $limit)->order('ji.score desc, ji.use_time asc, ji.id asc')->select();
            
        $joyItems = objToArray($joyItems);
        $rwhere['ji.txid'] = ['neq', ''];
        $ids = [];
        foreach($joyItems as $key => $item){
            $ids[] = $item['id'];
        }
        $rwhere['ji.id'] = ['in', $ids];
        $ranking = $this->getRankingByScore($where);
        foreach($joyItems as $key => $item){
            $mj_mdl = new MembersJoyModel();
            $total_medal_num = $mj_mdl->getTotalMedalNum();
            $member_mdl = new MembersModel();
            $joyItems[$key]['total_ela'] = $member_mdl->getBaseEla($total_medal_num) * $item['ela_medal_num'];
            $mt_mdl = new MemberTeamModel();
            $where = [
                'team_id' => $item['team_id'],
                'user_id' => $item['user_id'],
            ];
            $total_team_medal_num = $mj_mdl->getTotalMedalNumByTeamid($joyItems[$key]['team_id']);
            $joyItems[$key]['total_eco'] = $mt_mdl->getBaseEco($total_team_medal_num, $item['total_eco_currency']) * $mt_mdl->getMedalNumByWhere($where);
            $joyItems[$key]['ranking'] = array_search($item['score'], $ranking) + 1;
        }
        return $joyItems;
    }

    /**
     * 根据user_id,joy_id分页查询提交记录
     * @param $user_id 用户id
     * @param $joy_id 游戏id
     * @param $list_rows 每页数量
     * @param $page 当前页数
     */
    public function getJoyItemByUseridJoyid($user_id, $joy_id, $listRows, $page){
        $result = objToArray(
            $this->alias('ji')
                ->join('elastos_members m', 'ji.user_id=m.user_id')
                ->where('ji.user_id', $user_id)
                ->where('ji.joy_id', $joy_id)
                ->where('ji.txId', 'neq', '')
                ->order('ji.create_time desc')
                ->field('ji.id, ji.user_id, ji.team_id, ji.joy_id, ji.use_time, ji.score, ji.create_time, ji.plaintext, m.nickname, m.headimgurl')
                ->paginate($listRows, false, ['page' => $page])
        );
        return $result;
    }

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getTxblockDetailByWhere($where, $offset, $limit){
        $txblock = $this->alias('ji')->field('ji.*, m.nickname, m.headimgurl, m.openid, j.name as joy_name')
            ->join('elastos_members m', 'ji.user_id=m.user_id', 'LEFT')
            ->join('elastos_joy j', 'ji.joy_id=j.id', 'LEFT')
            ->where($where)->limit($offset, $limit)->order('ji.id desc')->select();

        $txblock = objToArray($txblock);
        $status = config('ranking_status');
        foreach($txblock as $key => $item){
            $txblock[$key]['status_desc'] = $status[$item['status']];
        }
        return $txblock;
    }

    /**
     * 根据条件更改一条记录
     */
    public function editOneJoyItemByWhere($where, $data){
        return $this->where($where)->update($data);
    }

    /**
     * 获取joy_id为在指定范围的 txid在指定范围的 得分最高的指定条记录
     */
    public function get_topscore($joy_ids, $txids, $num){
        $all = objToArray(
            $this->where('joy_id', 'in', $joy_ids)->where('txId', 'neq', '')->where('txId', 'in', $txids)->select()
        );
        $data = [];
        if($all){
            $user_ids = [];
            $team_joy_arr = [];
            foreach($all as $k => $v){
                $user_ids[] = $v['user_id'];
                $team_joy_arr[$v['user_id']] = [
                    'team_id' => $v['team_id'],
                    'joy_id' => $v['joy_id']
                ];
            }
            $user_ids = array_unique($user_ids);
            $scores = [];
            foreach($user_ids as $k => $v){
                $scores[$v] = 0;
                foreach($all as $k1 => $v1){
                    if($v1['user_id'] == $v){
                        $scores[$v] += $v1['score'];
                    }
                }
            }
            arsort($scores, SORT_NUMERIC);
            foreach($scores as $k => $v){
                $data[] = [
                    'user_id' => $k,
                    'team_id' => $team_joy_arr[$k]['team_id'],
                    'joy_id' => $team_joy_arr[$k]['joy_id'],
                    'total_score' =>$v
                ];
            }
            if(count($data) > $num){
                $result = [];
                for($i = 0; $i < $num; $i++){
                    $result[$i] = $data[$i];
                }
                return $result;
            }
        }
        return $data;
    }

    public function getRankingByScore($where){
        $ranking = $this->alias('ji')->where($where)->order('ji.score desc, ji.use_time asc, ji.id asc')->column('ji.score');
        $ranking = array_values($ranking);
        return $ranking;
    }

    /**
     * 根据txId获取user_id
     */
    public function getUseridByTxId($txId){
        return $this->where('txId', $txId)->value('user_id');
    }

    /**
     * 根据txId获取team_id
     */
    public function getTeamidByTxId($txId){
        return $this->where('txId', $txId)->value('team_id');
    }

    /**
     * 根据搜索条件获取列表信息
     * @param $joy_ids
     * @param $txids
     * @param $limit
     */
    public function getDailyTopInfo($joy_ids, $txids, $limit, $user_ids){
        $dailytop_result = [];
        if(!empty($user_ids)){
            $dailytop = $this->get_topscore($joy_ids, $txids, $limit);
            $ranking = $this->getDailyTopRanking($dailytop, $user_ids);
            foreach($dailytop as $key => $item){
                if(in_array($item['user_id'],$user_ids)){
                    $mj_mdl = new MembersJoyModel();
                    $total_medal_num = $mj_mdl->getTotalMedalNum();
                    $mj = $mj_mdl->getOneByUserid($item['user_id']);
                    $member_mdl = new MembersModel();
                    $member = $member_mdl->getOneMember($item['user_id']);
                    $dailytop[$key]['nickname'] = $member['nickname'];
                    $dailytop[$key]['headimgurl'] = $member['headimgurl'];
                    $dailytop[$key]['total_ela'] = $member_mdl->getBaseEla($total_medal_num) * $mj['medal_num'];
                    $dailytop[$key]['ranking'] = array_search($item['total_score'], $ranking) + 1;
                    $dailytop_result[] = $dailytop[$key];
                }
            }
        }
        return $dailytop_result;
    }

    public function getDailyTopRanking($dailytop, $user_ids){
        $ranking = [];
        foreach($dailytop as $key => $item){
            if(in_array($item['user_id'],$user_ids)){
                $ranking[] = $item['total_score'];
            }
        }
        return $ranking;
    }

}