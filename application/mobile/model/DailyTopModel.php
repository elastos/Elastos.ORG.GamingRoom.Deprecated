<?php
namespace app\mobile\model;

use think\Model;

class DailyTopModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_daily_top';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getDailyTopByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllDailyTop($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertDailyTop($param){
        try{

            $result =  $this->validate('DailyTopValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '添加成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editDailyTop($param){
        try{
            $result =  $this->save($param, ['id' => $param['id']]);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneDailyTop($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delDailyTop($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getDailyTopInfoByWhere($where, $offset, $limit){
        $dailytop = $this->alias('dt')->field('dt.*, m.nickname, m.openid, m.headimgurl, mj.team_id, mj.medal_num as ela_medal_num, t.name as teamname, t.logo, t.total_eco_currency')
            ->join('elastos_members m', 'dt.user_id=m.user_id')
            ->join('elastos_members_joy mj', 'dt.user_id=mj.user_id')
            ->join('elastos_teams t', 'mj.team_id=t.id','LEFT')
            ->where($where)->limit($offset, $limit)->order('dt.total_score desc, dt.id asc')->select();

        $dailytop = objToArray($dailytop);
        $ids = [];
        foreach($dailytop as $key => $item){
            $ids[] = $item['id'];
        }
        $rwhere['dt.id'] = ['in', $ids];
        $ranking = $this->getRankingByScore($rwhere);
        foreach($dailytop as $key => $item){
            $mj_mdl = new MembersJoyModel();
            $total_medal_num = $mj_mdl->getTotalMedalNum();
            $member_mdl = new MembersModel();
            $dailytop[$key]['total_ela'] = $member_mdl->getBaseEla($total_medal_num) * $item['ela_medal_num'];
            $dailytop[$key]['ranking'] = array_search($item['total_score'], $ranking) + 1;
        }
        return $dailytop;
    }

    public function getRankingByScore($where){
        $ranking = $this->alias('dt')->where($where)->order('dt.total_score desc')->column('dt.total_score');
        $ranking = array_values($ranking);
        return $ranking;
    }

    /**
     * 批量插入信息
     */
    public function insertMany($data){
        return $this->saveAll($data, false);
    }

    /**
     * 取出所有未发送奖励的记录
     */
    public function get_allUnReward(){
        return objToArray(
            $this->where('is_issue', 0)->select()
        );
    }

    /**
     * 取出所有已发送奖励但未发送消息的记录
     */
    public function get_allUnNoticed(){
        return objToArray(
            $this->where('is_issue', 1)->where('is_notice', 0)->select()
        );
    }

    /**
     * 根据txid获取id
     */
    public function getIdByTxid($txid){
        return $this->where('txid', $txid)->value('id');
    }

    /**
     * 批量更新记录
     */
    public function editByWhere($where, $param){
        return $this->where($where)->update($param);
    }

    /**
     * 根据cur_time和user_id查询当天是否插入过了
     */
    public function getOneByTimestampUserid($cur_time, $user_id){
        return $this->where('cur_time', $cur_time)->where('user_id', $user_id)->find();
    }
}