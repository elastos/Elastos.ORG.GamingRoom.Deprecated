<?php
namespace app\mobile\model;

use think\Model;

class TransferModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_transfer';

    /**
     * 插入一条记录
     */
    public function insertOneAndGetID($data){
        return $this->insertGetId($data);
    }

    /**
     * 根据id查询此次奖励是否已发送
     */
    public function getStatusById($id){
        return $this->where('id', $id)->value('status');
    }
}