<?php
namespace app\mobile\model;

use think\Model;

class MemberChargeModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_member_charge';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getMemberChargeByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllMemberCharge($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertMemberCharge($param){
        try{
            $result = $this->save($param);
            return false === $result ? false : true;
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editMemberCharge($param){
        try{
            $result =  $this->save($param, ['id' => $param['id']]);
            return false === $result ? false : true;
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id更新一条信息
     */
    public function updateOneById($id, $data){
        return $this->where('id', $id)->update($data);
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneMemberCharge($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delMemberCharge($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 获取所有未确认发送手续费的记录
     */
    public function get_AllUnissue(){
        return objToArray(
            $this->where('transfer_id', '>', 0)->where('is_issue', 0)->select()
        );
    }
}