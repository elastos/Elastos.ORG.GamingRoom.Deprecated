<?php
namespace app\mobile\model;

use think\Model;

class MsgModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_msg';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getMsgByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllMsg($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertMsg($param){
        try{

            $result =  $this->validate('MsgValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '添加成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editMsg($param){
        try{

            $result =  $this->validate('MsgValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '编辑成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneMsg($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delMsg($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据条件搜索分页消息
     */
    public function getMsgByTypeKeyword($user_id, $type='', $keyword='', $listRows, $page){
        $where = [];
        $where['receive_user_id'] = $user_id;
        if($type > 0 || $type === '0'){
            $where['type'] = $type;
        }
        if($keyword){
            $where['content'] = ['like', '%'.$keyword.'%'];
        }
        $result = objToArray(
            $this->where($where)
                ->order('create_time desc')
                ->paginate($listRows, false, ['page' => $page])
        );
        if(isset($result['data'])){
            $msg_types = config('msg_types');
            $msg_types_arr = [];
            foreach($msg_types as $k => $v){
                $msg_types_arr[$v['type']] = $v['type_name'];
            }
            foreach($result['data'] as $k => $v){
                $result['data'][$k]['type_name'] = $msg_types_arr[$v['type']];
            }
        }
        return $result;
    }

    /**
     * 插入一条信息
     */
    public function insertOne($data){
        return $this->insert($data);
    }
}