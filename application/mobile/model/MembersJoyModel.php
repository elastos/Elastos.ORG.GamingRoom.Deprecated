<?php
namespace app\mobile\model;

use think\Model;

class MembersJoyModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_members_joy';

    /**
     * 根据useiid获取一条记录
     */
    public function getOneByUserid($user_id){
        return $this->where('user_id', $user_id)->find();
    }

    /**
     * 根据openid获取一条记录
     */
    public function getOneByOpenid($openid){
        return $this->where('openid', $openid)->find();
    }

    /**
     * 新增一条数据
     */
    public function insertOne($data){
        return $this->save($data);
    }

    /**
     * 获取所有奖牌数
     */
    public function getTotalMedalNum(){
        $result = $this->select();
        $total_medal_num = 0;
        $result = objToArray($result);
        foreach($result as $key => $item){
            if(isset($item['medal_num']) && $item['medal_num'] > 0){
                $total_medal_num = $total_medal_num + $item['medal_num'];
            }
        }
        return $total_medal_num;
    }

    /**
     * 根据team_id获取所有奖牌数
     */
    public function getTotalMedalNumByTeamid($team_id){
        return array_sum($this->where('team_id', $team_id)->column('medal_num'));
    }

    /**
     * 根据team_id计算总人数
     */
    public function calculateMembersNumByTeamid($team_id){
        return $this->where('team_id', $team_id)->count();
    }

    /**
     * 根据条件更新一条记录
     */
    public function updateOneByWhere($where, $data){
        return $this->save($data, $where);
    }

    /**
     * 以medal_num逆序取出某team_id全部数据
     */
    public function getAllOrderbyMedalnumByTeamid($team_id, $keyword='', $listRows, $page){
        $where = [];
        $where['mj.team_id'] = $team_id;
        if($keyword){
            $where['m.nickname'] = ['like', '%'.$keyword.'%'];
        }
        $result =  objToArray(
            $this->alias('mj')
                ->join('elastos_members m', 'm.user_id=mj.user_id')
                ->where($where)
                ->field('m.user_id, m.nickname, m.headimgurl, mj.medal_num, mj.team_id')
                ->order('mj.medal_num desc')
                ->paginate($listRows, false, ['page' => $page])
        );
        ////获取排行//////
        $all = $this->where('team_id', $team_id)->order('medal_num desc')->column('medal_num');
        $all = array_values($all);
        foreach($result['data'] as $k => $v){
            $result['data'][$k]['ranking'] = array_search($v['medal_num'], $all) + 1;
        }
        ////结束/////////
        return $result;
    }

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getTotalByWhere($where, $offset, $limit){
        $members = $this->alias('mj')->field('mj.id, mj.user_id, mj.team_id, mj.medal_num as ela_medal_num, m.nickname, m.openid, m.headimgurl, t.name as teamname, t.logo, t.total_eco_currency')
            ->join('elastos_members m', 'mj.user_id=m.user_id')
            ->join('elastos_teams t', 'mj.team_id=t.id','LEFT')
            ->where($where)->limit($offset, $limit)->order('mj.medal_num desc, m.user_id asc')->select();

        $members = objToArray($members);
        $ranking = $this->getRankingByWhere([]);
        foreach($members as $key => $item){
            $member_mdl = new MembersModel();
            $members[$key]['total_ela'] = $member_mdl->getBaseEla($this->getTotalMedalNum()) * $item['ela_medal_num'];
            $members[$key]['ranking'] = array_search($item['ela_medal_num'], $ranking) + 1;
        }
        return $members;
    }

    public function getRankingByWhere($where){
        $ranking = $this->where($where)->order('medal_num desc')->column('medal_num');
        $ranking = array_values($ranking);
        return $ranking;
    }

    /**
     * 增加用户的ela奖牌数
     */
    public function add_medal_num($user_id, $num){
        return $this->where('user_id', $user_id)->setInc('medal_num', $num);
    }

    /**
     * 获取所有用户信息
     */
    public function getAll(){
        return objToArray(
            $this->select()
        );
    }

    /**
     * 根据userid更新信息
     */
    public function updateOneByUserid($user_id, $data){
        return $this->where('user_id', $user_id)->update($data);
    }
}