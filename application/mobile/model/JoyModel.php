<?php
namespace app\mobile\model;

use think\Model;

class JoyModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_joy';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getJoyByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllJoy($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertJoy($param){
        try{

            $result =  $this->validate('JoyValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '添加成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editJoy($param){
        try{

            $result =  $this->validate('JoyValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '编辑成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneJoy($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delJoy($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据名称获取信息
     * @param $name
     */
    public function findJoyByName($name){
        return $this->where('name', $name)->find();
    }

    /**
     * 获取所有游戏信息
     */
    public function getAllJoys(){
        return objToArray($this->select());
    }

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     */
    public function getAllJoyByWhere($where){
        return $this->where($where)->select();
    }

    /**
     * 获取所有支持发奖励的游戏id
     */
    public function getAllJoyidWhereisAward(){
        return $this->where('is_award', 1)->column('id');
    }
}