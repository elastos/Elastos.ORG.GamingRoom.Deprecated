<?php
namespace app\mobile\model;

use think\Model;

class MemberTeamModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_member_team';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getMemberTeamByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertMemberTeam($param){
        try{
            $result =  $this->save($param);
            return false === $result ? false : true;
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editMemberTeam($param){
        try{
            $result =  $this->save($param, ['id' => $param['id']]);
            return false === $result ? false : true;
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneMemberTeam($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delMemberTeam($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据条件获取信息
     * @param $where
     */
    public function getMemberTeamsByWhere($where){
        return $this->where($where)->select();
    }

    /**
     * 根据用户id获取各企业奖牌数
     */
    public function getMedalnumByUserid($userid){
        return $this->alias('mt')
                    ->join('elastos_teams t', 'mt.team_id=t.id')
                    ->where('mt.user_id', $userid)
                    ->column('t.name, mt.medal_num');
    }

    /**
     * 根据用户id和team_id获取id
     */
    public function getOneByUseridTeamid($user_id, $team_id){
        return $this->where('user_id', $user_id)->where('team_id', $team_id)->find();
    }

    /**
     * 获取生态币运算
     */
    public function getBaseEco($total_medal_num, $total_eco_currency){
        $base_eco = ($total_medal_num > 0) ? $total_eco_currency/$total_medal_num: $total_eco_currency;
        return $base_eco;
    }

    /**
     * 获取所有奖牌数
     */
    public function getTotalMedalNum($param){
        $where = [
            'team_id' => $param['team_id'],
        ];
        $result = $this->where($where)->select();
        $total_medal_num = 0;
        $result = objToArray($result);
        foreach($result as $key => $item){
            if(isset($item['medal_num']) && $item['medal_num'] > 0){
                $total_medal_num = $total_medal_num + $item['medal_num'];
            }
        }
        return $total_medal_num;
    }

    /**
     * 根据where获取奖牌数
     */
    public function getMedalNumByWhere($where){
        $mt = $this->where($where)->find();
        return $mt['medal_num'];
    }


    /**
     * 根据team_id获取本team当前总人数
     */
    public function getAllMembersNumByTeamid($team_id){
        $all_members = $this->where('team_id', $team_id)->column('user_id');
        return count(array_unique($all_members));
    }

    /**
     * 根据team_id获取本team当前总奖牌
     */
    public function getAllMemdalNumByTeamid($team_id){
        $all_memdals = $this->where('team_id', $team_id)->column('medal_num');
        return array_sum($all_memdals);
    }

    /**
     * 以medal_num逆序取出某team_id全部数据
     */
    public function getAllOrderbyMedalnumByTeamid($team_id, $keyword='', $listRows, $page){
        $where = [];
        $where['mt.team_id'] = $team_id;
        if($keyword){
            $where['m.nickname'] = ['like', '%'.$keyword.'%'];
        }
        return objToArray(
            $this->alias('mt')
                ->join('elastos_members m', 'm.user_id=mt.user_id')
                ->join('elastos_members_joy mj', 'mj.user_id=mt.user_id')
                ->where($where)
                ->field('m.user_id, m.nickname, m.headimgurl, mj.medal_num, mt.id as member_team_id, mt.team_id, mt.wallet_addr')
                ->order('mj.medal_num desc, mt.id')
                ->paginate($listRows, false, ['page' => $page])
        );
    }

    /**
     * 增加用户的奖牌数
     */
    public function add_medal_num($user_id, $team_id, $num){
        return $this->where('user_id', $user_id)->where('team_id', $team_id)->setInc('medal_num', $num);
    }

    /**
     * 根据用户id和战队id获取钱包地址
     */
    public function getOneWalletAddr($user_id, $team_id){
        return $this->where('user_id', $user_id)->where('team_id', $team_id)->value('wallet_addr');
    }

}