<?php
namespace app\mobile\model;

use think\Model;

class MembersModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_members';
    // 自动更新时间
    // protected $autoWriteTimestamp = true;

    /**
     * 根据搜索条件获取用户列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getMembersByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')->where($where)->limit($offset, $limit)->order('user_id desc')->select();
    }

    /**
     * 根据搜索条件获取所有的用户数量
     * @param $where
     */
    public function getAllMembers($where=[]){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertMember($param){
        try{
            $result =  $this->save($param);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editMember($param){
        try{
            $result =  $this->save($param, ['user_id' => $param['id']]);
            if(false === $result){
                return false;
            }else{
                return true;
            }
        }catch(PDOException $e){
            return false;
        }
    }

    /**
     * 根据id获取角色信息
     * @param $id
     */
    public function getOneMember($id){
        return $this->where('user_id', $id)->find();
    }

    /**
     * 根据openid获取一个角色信息
     */
    public function getOneMemberByOpenid($openid){
        return objToArray($this->where('openid', $openid)->find());
    }

    /**
     * 删除
     * @param $id
     */
    public function delMember($id){
        try{

            $this->where('user_id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据用户名获取信息
     * @param $name
     */
    public function findMemberByName($name){
        return $this->where('nickname', $name)->find();
    }

    /**
     * 根据手机获取信息
     * @param $mobile
     */
    public function findMemberByMobile($mobile){
        return $this->where('mobile', $mobile)->find();
    }

    public function editSingleMember($params){
        return $this->save($params, ['user_id' => $params['user_id']]);
    }

    public function editSingleMemberByNickname($nickname, $params){
        return $this->where('nickname', $nickname)->update($params);
    }

    /**
     * 根据用户ident_code获取用户名
     */
    public function getNicknameByIdentcode($ident_code){
        return $this->where('ident_code', $ident_code)->value('nickname');
    }

    /**
     * 根据用户ident_code获取用户id
     */
    public function getUseridByIdentcode($ident_code){
        return $this->where('ident_code', $ident_code)->value('user_id');
    }

    /**
     * 根据用户openid获取用户id
     */
    public function getUseridByOpenid($openid){
        return $this->where('openid', $openid)->value('user_id');
    }

    /**
     * 获取所有用户的id列表
     */
    public function getAllUserids(){
        return $this->column('user_id');
    }

    /**
     * 根据team_id计算总人数
     */
    public function calculateMembersNumByTeamid($team_id){
        return $this->where('team_id', $team_id)->count();
    }

    /**
     * 获取ela运算
     */
    public function getBaseEla($total_medal_num){
        $total_ela_currency = $this->getTotalEla();
        $base_ela = ($total_medal_num > 0) ? $total_ela_currency/$total_medal_num: $total_ela_currency;
        return $base_ela;
    }

    /**
     * 获取ela运算
     */
    public function getTotalEla(){
        $count = $this->getAllMembers();
        $total_ela_currency = ($count < 1000) ? 100: $count * 0.1;
        return $total_ela_currency;
    }

    /**
     * 根据条件获取所有user_id
     */
    public function getUseridByWhere($where){
        $user_ids = $this->where($where)->order('user_id asc')->column('user_id');
        $user_ids = array_values($user_ids);
        return $user_ids;
    }
}