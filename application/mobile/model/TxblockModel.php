<?php
namespace app\mobile\model;

use think\Model;

class TxblockModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_txblock';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getTxblockByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllTxblock($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertTxblock($param){
        try{

            $result =  $this->validate('TxblockValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '添加成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editTxblock($param){
        try{

            $result =  $this->validate('TxblockValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '编辑成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneTxblock($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delTxblock($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getTxblockInfoByWhere($where, $offset, $limit){
        if($where['txid']){
            $where['tx.txid'] = $where['txid'];
            unset($where['txid']);
        }
        
        $txblock = $this->alias('tx')->field('tx.*, ji.user_id, ji.score, ji.joy_id, ji.status, m.nickname, m.openid, m.headimgurl, m.team_id, m.medal_num as ela_medal_num, t.name as teamname, t.logo, t.total_eco_currency')
            ->join('elastos_joy_item ji', 'tx.txid=ji.txId')
            ->join('elastos_members m', 'ji.user_id=m.user_id')
            ->join('elastos_teams t', 'ji.team_id=t.id')
            ->where($where)->limit($offset, $limit)->order('tx.id asc')->select();

        $txblock = objToArray($txblock);
        foreach($txblock as $key => $item){
            $member_mdl = new MembersModel();
            $txblock[$key]['total_ela'] = $member_mdl->getBaseEla() * $item['ela_medal_num'];
        }
        return $txblock;
    }

    /**
     * 根据区块height获取此区块所有交易记录的信息 
     */
    public function getAllTransactionsDetailByHeight($height, $listRows, $page){
        $result = objToArray(
            $this->alias('txb')
                ->join('elastos_joy_item ji', 'txb.txid=ji.txId')
                ->join('elastos_members m', 'ji.user_id=m.user_id')
                ->where('txb.height', $height)
                ->where('ji.joy_id', 1)
                ->order('txb.timestamp desc')
                ->field('m.nickname, ji.score, ji.create_time, ji.status')
                ->paginate($listRows, false, ['page' => $page])
        );
        return $result;
    }

    /**
     * 取出所有未存储的区块记录并处理
     */
    public function getDataForHeightBlocks($end_block){
        //所有未存储的区块的交易记录
        $height_blocks = $this->where('height', '>', $end_block)->order('height asc')->column('height');
        $data = [];
        if($height_blocks){
            if($end_block == 0){
                $end_block = $height_blocks[0] - 1;
            }
            for($i = $end_block + 1; $i <= end($height_blocks); $i++){
                if(in_array($i, $height_blocks)){
                    $data[] = [
                        'height' => $i,
                        'timestamp' => $this->get_block_timestamp($i),
                        'transaction_num' => count($this->getAllJoyTxidByHeight($i)),
                        'rewarded' => 0,
                        'create_time' => time()
                    ];
                }else{
                    $data[] = [
                        'height' => $i,
                        'timestamp' => 0,
                        'transaction_num' => 0,
                        'rewarded' => 0,
                        'create_time' => time()
                    ];
                }
            }
        }
        return $data;
    }

    /**
     * 根据区块高度获取其出块时间
     */
    public function get_block_timestamp($height){
        return $this->where('height', $height)->value('timestamp');
    }

    /**
     * 取出某个时间段内的所有区块 txid信息
     */
    public function getAllTxidBetweenTime($begin, $end){
        return $this->where('timestamp', 'between', [$begin, $end])->column('txid');
    }

    /**
     * 取出某个区块下的所有交易txid
     */
    public function getAllTxidByHeight($height){
        return $this->where('height', $height)->column('txid');
    }

    /**
     * 取出某个区块下的所有游戏提交的交易txid
     */
    public function getAllJoyTxidByHeight($height){
        $joy_item_mdl = new JoyItemModel();
        $txids =  $this->where('height', $height)->column('txid');
        return $joy_item_mdl->where('txId', 'neq', '')->where('txId', 'in', $txids)->where('joy_id', 1)->column('txId');
    }
}