<?php
namespace app\mobile\model;

use think\Model;

class HeightBlocksModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_height_blocks';

    /**
     * 根据height获取此区块信息
     */
    public  function getOneBlock($height){
        return $this->where('height', $height)->find();
    }

    /**
     * 分页获取所有区块
     */
    public function getAllBlocks($listRows, $page){
        return objToArray(
            $this->order('height desc')
                ->paginate($listRows, false, ['page' => $page])
        );
    }

    /**
     * 按条件获取所有已存区块高度
     */
    public function getAllBlockHeights($where = null){
        return $this->where($where)->order('height asc')->column('height');
    }

    /**
     * 取出未出奖且有交易记录的区块
     */
    public function getUnrewardedBlocks(){
        return $this->where('rewarded', 0)->where('transaction_num', '>', 0)->column('height');
    }

    /**
     * 获取区块表中最新的区块
     */
    public function getEndBlock(){
        return $this->order('height desc')->value('height');
    }

    /**
     * 批量插入信息
     */
    public function insertMany($data){
        return $this->saveAll($data, false);
    }

    /**
     * 批量更新记录
     */
    public function editOneByWhere($where, $param){
        return $this->where($where)->update($param);
    }
}