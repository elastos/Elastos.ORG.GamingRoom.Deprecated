<?php
namespace app\mobile\model;

use think\Model;

class TeamsModel extends Model{
    // 确定链接表名
    protected $table = 'elastos_teams';

    /**
     * 根据搜索条件获取列表信息
     * @param $where
     * @param $offset
     * @param $limit
     */
    public function getTeamsByWhere($where, $offset, $limit){
        return $this->field($this->table . '.*')
            ->where($where)->limit($offset, $limit)->order('id desc')->select();
    }

    /**
     * 根据搜索条件获取数量
     * @param $where
     */
    public function getAllTeamsNum($where){
        return $this->where($where)->count();
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertTeam($param){
        try{

            $result =  $this->validate('TeamValidate')->save($param);
            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '添加成功');
            }
        }catch(PDOException $e){

            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editTeam($param){
        try{

            $result =  $this->validate('TeamValidate')->save($param, ['id' => $param['id']]);

            if(false === $result){
                // 验证失败 输出错误信息
                return msg(-1, '', $this->getError());
            }else{

                return msg(1, url('index/index'), '编辑成功');
            }
        }catch(PDOException $e){
            return msg(-2, '', $e->getMessage());
        }
    }

    /**
     * 根据id获取信息
     * @param $id
     */
    public function getOneTeam($id){
        return $this->where('id', $id)->find();
    }

    /**
     * 删除
     * @param $id
     */
    public function delTeam($id){
        try{

            $this->where('id', $id)->delete();
            return msg(1, '', '删除成功');

        }catch( PDOException $e){
            return msg(-1, '', $e->getMessage());
        }
    }

    /**
     * 根据名称获取信息
     * @param $name
     */
    public function findTeamByName($name){
        return $this->where('name', $name)->find();
    }

    /**
     * 根据名称获取id
     * @param $name
     */
    public function getTeamIdByName($name){
        return $this->where('name', $name)->value('id');
    }

    /**
     * 获取所有有效team的id
     */
    public function getAllTeamsId(){
        return $this->where('is_valid', 1)->column('id');
    }

    /**
     * 获取所有有效team
     */
    public function getAllTeams(){
        return objToArray($this->where('is_valid', 1)->select());
    }

    /**
     * 所有生态企业个人关联team信息
     */
    public function getAllTeamsWithUserInfo($userid){
        $member_team_mdl = new MemberTeamModel();
        $teams = objToArray($this->where('is_valid', 1)->select());
        foreach($teams as $k => $v){
            $wallet_addr = $member_team_mdl->getOneWalletAddr($userid, $v['id']);
            $teams[$k]['wallet_addr'] = $wallet_addr ? $wallet_addr : null;
            $teams[$k]['create_url'] = config('create_wallet_url.'.$v['name']);
        }
        return $teams;
    }

    /**
     * 获取当前team的生态币总额
     */
    public function getTotalecocurrencyByTeamid($team_id){
        return $this->where('id', $team_id)->value('total_eco_currency');
    }
}