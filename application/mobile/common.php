<?php
/**
 * 检验登陆
 * @param
 * @return bool
 */
function isLogin(){
    if(session('userid') && session('userid') > 0){
        return session('userid');
    }else{
        return false;
    }
}

/**
 * 统一返回信息
 * @param $code
 * @param $data
 * @param $msge
 */
function msg($code, $data, $msg){
    return compact('code', 'data', 'msg');
}

/**
 * 对象转换成数组
 * @param $obj
 */
function objToArray($obj){
    return json_decode(json_encode($obj), true);
}

//判断浏览器中英文
function getPreferredLanguage() {  
    $langs = array();  
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) { //浏览器语言
        preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)s*(;s*qs*=s*(1|0.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);  
        if (count($lang_parse[1])) {
            $langs = array_combine($lang_parse[1], $lang_parse[4]);  
             
            foreach ($langs as $lang => $val) {  
                if ($val === '') $langs[$lang] = 1;  
            }  
              
            arsort($langs, SORT_NUMERIC);  
        }  
    }  
    
    foreach ($langs as $lang => $val) { break; }  
    if (stristr($lang,"-")) {$tmp = explode("-",$lang); $lang = $tmp[0]; }  
    return $lang;  
}

// 随机生成6位唯一识别码
function randCodeCN($len = 6){
    $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    mt_srand((double)microtime()*1000000*getmypid());
    $code = '';
    while(strlen($code) < $len)
        $code .= substr($chars, (mt_rand()%strlen($chars)), 1);
    return $code;
}

function curl_get_https($url){ // 模拟提交数据函数
    $curl = curl_init(); // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    $tmpInfo = curl_exec($curl); // 执行操作
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);//捕抓异常
    }
    curl_close($curl); // 关闭CURL会话
    return $tmpInfo; // 返回数据，json格式
}

function curl_post_https($url,$data){ // 模拟提交数据函数
    $curl = curl_init(); // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    $tmpInfo = curl_exec($curl); // 执行操作
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);//捕抓异常
    }
    curl_close($curl); // 关闭CURL会话
    return $tmpInfo; // 返回数据，json格式
}

function createToken(){
    $token = session('ident_code').time().mt_rand(0,1000000); 
    session('token'.session('ident_code'), authcode($token));
        
    return true;
}

function authcode($str) {
    return md5($str);
}

function checkToken($token) {
    if ($token == session('token'.session('ident_code'))) {
        session('token'.session('ident_code'), null);
        return true;
    } else {
        return false;
    }
}

function hashCode($str){
    return hash('sha256', $str);
}

/**
 * 拼装菜单
 * @return array
 */
function makeMenus(){
    $menus = [
        'ChinaJoy奖赏榜' => [
            'href' => '',
        ],
        '游戏排行榜' => [
            'href' => '',
        ],
        '个人中心' => [
            'href' => url('member/index'),
        ],
        '游戏大厅' => [
            'href' => '',
        ],
    ];
    return $menus;
}
