<?php
namespace app\mobile\controller;

use app\mobile\model\MembersModel;
use app\mobile\model\MembersJoyModel;
use app\mobile\model\TransferModel;
use app\mobile\model\MemberChargeModel;


class Index extends Base{
    public function index(){
        $members_mdl = new MembersModel();
        // 接收用户openid
        $openid = input('param.openid');
        // 用户信息
        $userInfo = $members_mdl->getOneMemberByOpenid($openid);
        // 用户信息存入session
        session('user_id', $userInfo['user_id']);
        cookie('user_id', $userInfo['user_id']);
        // 处理用户信息
        $this->dealMembersJoy($userInfo);
        // 重定向到游戏首页
        $this->redirect(url('joy/index'));
    }

    /**
     * 获取到最新用户信息后，与members表进行交互
     */
    private function dealMembersJoy($userInfo){
        $members_joy_mdl = new MembersJoyModel();
        //检查是否已经存在记录
        $check = $members_joy_mdl->getOneByUserid($userInfo['user_id']);
        if(!$check){
            $result = $members_joy_mdl->save(['user_id' => $userInfo['user_id'], 'openid' => $userInfo['openid']]);
            //如果已经有ela钱包地址的话 奖励一笔手续费
            // if($userInfo['wallet_addr']){
            //     $transfer_mdl = new TransferModel();
            //     $data = [];
            //     $data['wallet_addr'] = $userInfo['wallet_addr'];
            //     $data['amount'] = config('charge_amount');
            //     $transfer_id = $transfer_mdl->insertOneAndGetID($data);
            //     if($transfer_id > 0){
            //         //添加member_charge表记录
            //         $member_charge_mdl = new MemberChargeModel();
            //         $charge_data = [];
            //         $charge_data['user_id'] = $userInfo['user_id'];
            //         $charge_data['wallet_addr'] = $userInfo['wallet_addr'];
            //         $charge_data['is_first'] = 1;
            //         $charge_data['transfer_id'] = $transfer_id;
            //         $charge_data['create_time'] = time();
            //         $charge_data['update_time'] = time();
            //         $member_charge_mdl->insertMemberCharge($charge_data);
            //     }
            // }
        }else{
            $result = true;
        }
        return $result;
    }
}
