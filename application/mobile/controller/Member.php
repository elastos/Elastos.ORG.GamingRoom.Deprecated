<?php
namespace app\mobile\controller;

use app\mobile\model\MembersModel;
use think\exception\DbException;
use app\mobile\model\MemberTeamModel;
use app\mobile\model\TeamsModel;
use app\mobile\model\TeamSwitchLogModel;
use app\mobile\model\JoyItemModel;
use app\mobile\model\JoyModel;
use app\mobile\model\MsgModel;
use app\mobile\model\MembersJoyModel;
use app\mobile\model\TransferModel;
use app\mobile\model\MemberChargeModel;
use app\mobile\validate\MembersJoyValidate;


class Member extends Base{
    /**
     * 获取当前用户信息
     */
    private function get_memberInfo(){
        $members_mdl = new MembersModel();
        $members_joy_mdl = new MembersJoyModel();
        $user_id = session('?user_id') ? session('user_id') : 0;
        if(!$user_id) $user_id = cookie('user_id');
        $memberInfo = $members_mdl->getOneMember($user_id);
        $memberInfo['headimgurl'] = str_replace('http://', 'https://', $memberInfo['headimgurl']);
        $member_joy_info = $members_joy_mdl->getOneByUserid($user_id);
        $memberInfo['team_id'] = $member_joy_info['team_id'];
        $memberInfo['medal_num'] = $member_joy_info['medal_num'];
        return $memberInfo;
    }

    /**
     * 个人中心主页
     */
    public function index(){
        $member_team_mdl = new MemberTeamModel();
        $teams_mdl = new TeamsModel();
        
        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        //会员所属战队
        if($memberInfo['team_id'] > 0){
            $teamInfo = $teams_mdl->getOneTeam($memberInfo['team_id']);
        }else{
            $teamInfo = null;
        }

        //会员已获得奖牌数
        $medal_num = $memberInfo['medal_num'];

        //会员已获得ela币
        $ela_currency = $this->calculateElaCurrency();

        //会员已获得生态币 只计算当前所属战队
        if($memberInfo['team_id'] > 0){
            $team_currency = $this->calculateTeamCurrency($memberInfo['team_id']);
        }else{
            $team_currency = number_format(0, 6);
        }

        $this->assign([
            'memberInfo' => $memberInfo,
            'teamInfo' => $teamInfo,
            'medal_num' => $medal_num,
            'ela_currency' => $ela_currency,
            'team_currency' => $team_currency
        ]);

        return $this->fetch();
    }

    /**
     * 设置钱包地址页面
     */
    public function set_wallet_addr(){
        $teams_mdl = new TeamsModel();

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        //ELA个人相关信息
        $ela = [
            'name' => 'ELA',
            'wallet_addr' => $memberInfo['wallet_addr'],
            'create_url' => config('create_wallet_url.ELA'),
        ];

        //所有生态企业个人关联team信息
        $teams = $teams_mdl->getAllTeamsWithUserInfo($memberInfo['user_id']);
        

        $this->assign([
            'ela' => $ela,
            'team1' => $teams[0],
            'team2' => $teams[1],
            'team3' => $teams[2],
            'team4' => $teams[3],
        ]);

        return $this->fetch();
    }

    /**
     * 设置钱包地址动作
     */
    public function doSetWalletAddr(){
        $teams_mdl = new TeamsModel();
        $info = input('param.');

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        // ela wallet_addr处理
        $ela = $info[0];
        if(!$ela['wallet_addr']){
            return json(['status' => 0, 'msg' => 'ELA钱包地址不能为空!']);
        }
        if($ela['wallet_addr'] && $memberInfo['wallet_addr'] != $ela['wallet_addr']){
            $members_mdl = new MembersModel();
            $update = [];
            $update['id'] = $memberInfo['user_id'];
            $update['wallet_addr'] = $ela['wallet_addr'];
            $result = $members_mdl->editMember($update);
            //如果更新成功且之前没有钱包地址 奖励一笔手续费
            // if($result && !$memberInfo['wallet_addr']){
            //     $transfer_mdl = new TransferModel();
            //     $data = [];
            //     $data['wallet_addr'] = $ela['wallet_addr'];
            //     $data['amount'] = config('charge_amount');
            //     $transfer_id = $transfer_mdl->insertOneAndGetID($data);
            //     if($transfer_id > 0){
            //         //添加member_charge表记录
            //         $member_charge_mdl = new MemberChargeModel();
            //         $charge_data = [];
            //         $charge_data['user_id'] = $memberInfo['user_id'];
            //         $charge_data['wallet_addr'] = $ela['wallet_addr'];
            //         $charge_data['is_first'] = 1;
            //         $charge_data['transfer_id'] = $transfer_id;
            //         $charge_data['create_time'] = time();
            //         $charge_data['update_time'] = time();
            //         $member_charge_mdl->insertMemberCharge($charge_data);
            //     }
            // }
            //更新失败
            if(!$result){
                return json(['status' => 0, 'msg' => '绑定失败!']);
            }
        }

        //生态企业 wallet_addr处理
        foreach($info as $k => $v){
            if($k > 0){
                $result_team = $this->dealMemberTeamWallerAddr($v['team_id'], $v['wallet_addr']);
                if(!$result_team){
                    return json(['status' => 0, 'msg' => '绑定失败!']);
                }
            }
        }

        return json(['status' => 1, 'msg' => '绑定成功!']);
    }

    /**
     * 切换战队页面
     */
    public function switch_team(){
        $members_joy_mdl = new MembersJoyModel();
        $teams_mdl = new TeamsModel();

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        //当前战队id
        $current_team_id = $memberInfo['team_id'];
        
        //获取所有的team信息
        $teams = $teams_mdl->getAllTeams();
        foreach($teams as $k => $v){
            $teams[$k]['current_members_num'] = $members_joy_mdl->calculateMembersNumByTeamid($v['id']);
        }

        $this->assign([
            'current_team_id' => $current_team_id,
            'teams' => $teams
        ]);
        return $this->fetch();
    }

    /**
     * 切换战队动作
     */
    public function doSwitchTeam(){
        $member_team_mdl = new MemberTeamModel();
        $members_joy_mdl = new MembersJoyModel();
        $team_switch_log_mdl = new TeamSwitchLogModel();

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        //更改前的战队id
        $old_team_id = $memberInfo['team_id'];
        //要更改的战队id
        $team_id = input('param.team_id');

        //如果要更改的战队id与当前战队id相同，直接返回成功
        if($team_id == $old_team_id){
            return json(['status' => 0, 'msg' => '您没有更换战队!']);
        }

        //更新战队id
        $result = $members_joy_mdl->updateOneByWhere(['user_id' => $memberInfo['user_id']], ['team_id' => $team_id]);
        
        //更新成功记录日志
        if($result !== false){
            $add = [];
            $add['user_id'] = $memberInfo['user_id'];
            $add['exit_team_id'] = $old_team_id;
            $add['join_team_id'] = $team_id;
            $add['exit_time'] = time();
            $add['join_time'] = time();
            $team_switch_log_mdl->insertTeamLog($add);
        }
        if($result !== false){
            return json(['status' => 1, 'msg' => '切换战队成功!']);
        }else{
            return json(['status' => 0, 'msg' => '切换战队失败!']);
        }
    }

    /**
     * 个人成绩页面
     */
    public function personal_game_score(){
        $joy_mdl = new JoyModel();

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        //所有游戏列表 做tab项
        $joys = $joy_mdl->getAllJoys();
        $this->assign([
            'joys' => $joys,
            'joys_json' => json_encode($joys)
        ]);
        return $this->fetch();
    }

    /**
     * 获取个人成绩相关信息
     */
    public function ajax_getPersonalGameScore(){
        $joy_item_mdl = new JoyItemModel();
        
        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        $joy_id = input('param.joy_id');
        $page = input('param.page') ? input('param.page') : 1;

        $data = $joy_item_mdl->getJoyItemByUseridJoyid($memberInfo['user_id'], $joy_id, 20, $page);
        $data['hasNext'] = $data['current_page'] < $data['last_page'] ? true : false;
        
        return json($data);
    }

    /**
     * 我的消息页面
     */
    public function personal_message(){
        $msg_types = config('msg_types');

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        $this->assign([
            'msg_types' => $msg_types
        ]);
        return $this->fetch();
    }

    /**
     * 根据条件搜索我的消息
     */
    public function ajax_getPersonalMessage(){
        $msg_mdl = new MsgModel();

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        $type = input('param.msg_type');
        $keyword = input('param.keyword');
        $page = input('param.page') ? input('param.page') : 1;

        $data = $msg_mdl->getMsgByTypeKeyword($memberInfo['user_id'], $type, $keyword, 20, $page);
        $data['hasNext'] = $data['current_page'] < $data['last_page'] ? true : false;

        return json($data);
    }

    /**
     * 新增或更新当前用户某个生态企业的waller_addr
     */
    private function dealMemberTeamWallerAddr($team_id, $wallet_addr){
        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        $member_team_mdl = new MemberTeamModel();
        $exist_info = $member_team_mdl->getOneByUseridTeamid($memberInfo['user_id'], $team_id);
        if($exist_info){
            if(isset($exist_info['wallet_addr']) && $exist_info['wallet_addr'] == $wallet_addr){
                $result = true;
            }else{
                $exist_id = $exist_info['id'];
                $update = [];
                $update['id'] = $exist_id;
                $update['wallet_addr'] = $wallet_addr;
                $result = $member_team_mdl->editMemberTeam($update);
                if(!$result){
                    throw new DbException('当前用户更新生态企业wallet_addr失败');
                }
            }
        }else{
            $add = [];
            $add['user_id'] = $memberInfo['user_id'];
            $add['team_id'] = $team_id;
            $add['wallet_addr'] = $wallet_addr;
            $result = $member_team_mdl->insertMemberTeam($add);
            if(!$result){
                throw new DbException('当前用户新增生态企业wallet_addr失败');
            }
        }
        return $result;
    }

    /**
     * 根据用户id计算其当前获得的ela币数量
     */
    private function calculateElaCurrency(){
        $members_joy_mdl = new MembersJoyModel();
        $members_mdl = new MembersModel();

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        $total_ela_medal_num = $members_joy_mdl->getTotalMedalNum();
        $base_ela = $members_mdl->getBaseEla($total_ela_medal_num);
        return number_format($memberInfo['medal_num'] * $base_ela, 6);
    }

    /**
     * 根据用户id和team_id计算其获得的生态企业币数量
     */
    private function calculateTeamCurrency($team_id){
        $members_joy_mdl = new MembersJoyModel();
        $teams_mdl = new TeamsModel();
        $member_team_mdl = new MemberTeamModel();

        //当前用户信息
        $memberInfo = $this->get_memberInfo();

        $total_eco_medal_num = $members_joy_mdl->getTotalMedalNumByTeamid($team_id);
        $total_eco_currency = $teams_mdl->getTotalecocurrencyByTeamid($team_id);
        $base_eco = $member_team_mdl->getBaseEco($total_eco_medal_num, $total_eco_currency);
        return number_format($memberInfo['medal_num'] * $base_eco, 6);
    }

    /**
     * 统计所有人币奖励
     */
    public function calculateAllReward(){
        $members_joy_mdl = new MembersJoyModel();
        $members_mdl = new MembersModel();
        $teams_mdl = new TeamsModel();
        $member_team_mdl = new MemberTeamModel();
        //获取所有用户
        $members = $members_joy_mdl->getAll();

        foreach($members as $k => $v){
            // ela币
            $total_ela_medal_num = $members_joy_mdl->getTotalMedalNum();
            $base_ela = $members_mdl->getBaseEla($total_ela_medal_num);
            $ela_currency = number_format($v['medal_num'] * $base_ela, 6);

            // 生态币
            $eco_currency = 0;
            if($v['team_id'] > 0){
                $total_eco_medal_num = $members_joy_mdl->getTotalMedalNumByTeamid($v['team_id']);
                $total_eco_currency = $teams_mdl->getTotalecocurrencyByTeamid($v['team_id']);
                $base_eco = $member_team_mdl->getBaseEco($total_eco_medal_num, $total_eco_currency);
                $eco_currency = number_format($v['medal_num'] * $base_eco, 6);
            }

            // 存入members_joy表
            $data = [];
            $data['ela_currency'] = $ela_currency;
            $data['eco_currency'] = $eco_currency;
            $members_joy_mdl->updateOneByUserid($v['user_id'], $data);
        }
    }
}
