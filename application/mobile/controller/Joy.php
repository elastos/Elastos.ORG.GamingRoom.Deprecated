<?php
namespace app\mobile\controller;

use app\mobile\model\JoyModel;
use app\mobile\model\JoyItemModel;
use app\mobile\model\MembersModel;
use app\mobile\model\MemberTeamModel;
use app\mobile\model\MembersJoyModel;

class Joy extends Base{
    
    public function index(){
        $this->assign([
            'joys' => $this->getJoys(),
        ]);
        return $this->fetch();
    }

    public function results(){
        $joy_id = input('param.joy_id') ? input('param.joy_id'): 1;
        $user_id = session('?user_id') ? session('user_id') : 0;
        if(!$user_id) $user_id = cookie('user_id');
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $joy_url = config('joy_url');
        $this->assign([
            'joy_url' => $http_type . $_SERVER['HTTP_HOST'] . $joy_url[$joy_id] . '?uid=' . $user_id . '&serverurl='.$http_type . $_SERVER['HTTP_HOST'] . '/api/joy/syncJoySubmit',
        ]);
        return $this->fetch();
    }

    public function rules(){
        return $this->fetch();
    }

    public function ranking_list(){
        $joy_id = input('param.joy_id') ? input('param.joy_id'): 1;

        $this->assign([
            'tabs' => $this->getJoys(),
            'cur_tab' => $joy_id,
            'type' => 'pullon',
        ]);
        return $this->fetch(); 
    }

    private function getJoys(){ 
        $joy_mdl = new JoyModel();
        $joys = $joy_mdl->getAllJoyByWhere([]);
        $joys = objToArray($joys);
        $joy_url = config('joy_url');
        $user_id = session('?user_id') ? session('user_id') : 0;
        if(!$user_id) $user_id = cookie('user_id');
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        foreach($joys as $key => $joy){
            $joys[$key]['joy_url'] = $http_type . $_SERVER['HTTP_HOST'] . $joy_url[$joy['id']] . '?uid=' . $user_id . '&serverurl=' . $http_type . $_SERVER['HTTP_HOST'] . '/api/joy/syncJoySubmit';
        }
        return $joys;
    }

    public function ajax_getJoyItem(){
        $joy_id = input('param.joy_id') ? input('param.joy_id'): 1;
        $page = input('param.page') ? input('param.page'): 1;
        $limit = input('param.pageNum') ? input('param.pageNum'): 10;
        $offset = ($page - 1) * $limit;
        $joyItem = $this->getJoyItems($joy_id, $limit, $offset);
        foreach($joyItem as $key => $item){
            $joyItem[$key]['create_time'] = date('Y/m/d H:i:s', $item['create_time']);
        }

        return json($joyItem);
    }

    private function getJoyItems($joy_id, $limit, $offset){
        $joyItem_mdl = new JoyItemModel();
        $where = [
            'ji.joy_id' => $joy_id,
            'ji.txId' => ['neq', ''],
        ];
        $joyItem = $joyItem_mdl->getJoyItemByWhere($where, $limit, $offset);
        return objToArray($joyItem);
    }

    public function sys_error(){
        $this->assign([
            'title' => 404
        ]);
        return $this->fetch();
    }

}
