<?php
namespace app\mobile\controller;

use think\Controller;
use think\Lang;
use app\mobile\model\MembersModel;

class Base extends Controller{
    public function _initialize(){
        ///////////////////////////模拟登录数据////////////////////////////////
        // session('userid', 1);
        // session('openid', 'oMA9lv5IIaqMEIkfoarXCbTfRc7o');
        // session('nickname', 'Forrest');
        // session('ident_code', 'iipgqr');
        // ///////////////////////////模拟结束   ////////////////////////////////
        // if(!isLogin()){
        //     //未登录处理
        // }
        
        // $this->initLang();
        $control = lcfirst(request()->controller());
        $action = lcfirst(request()->action());
        $all_title = config('header_title');
        if(isset($all_title[$control][$action])){
            $this->assign([
                'title' => $all_title[$control][$action],
            ]);
        }
        

        // if(session('userid') && session('userid') > 0){
        //     $this->assign([
        //         'userid' => session('userid'),
        //         'nickname' => session('nickname'),
        //         'openid' => session('openid'),
        //     ]);
        // }
    }

    protected function getLangMsg($key){
        return Lang::get($key);
    }

    public function getIdentCode(){
        $ident_code = '';
        $member_mdl = new MembersModel();
        do{
            $ident_code = randCodeCN();
        }while($member_mdl->getAllMembers(['ident_code'=>$ident_code]));

        return $ident_code;
    }

    protected function initLang(){
        if(isset($_COOKIE['think_var'])){
            $lang = $_COOKIE['think_var'];
            Lang::setAllowLangList(config('lang_list'));
            Lang::detect();
            Lang::load(APP_PATH.DS.'mobile'.DS.'lang'.DS.$lang.EXT,$lang);
        }
    }
}