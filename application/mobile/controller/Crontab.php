<?php
namespace app\mobile\controller;

use app\mobile\model\HeightBlocksModel;
use app\mobile\model\TxblockModel;
use app\mobile\model\JoyItemModel;
use app\mobile\model\JoyModel;
use app\mobile\model\DailyTopModel;
use app\mobile\model\MembersModel;
use app\mobile\model\MemberTeamModel;
use app\mobile\model\MsgModel;
use app\mobile\model\MembersJoyModel;
use app\mobile\model\MemberChargeModel;
use app\mobile\model\TransferModel;


class Crontab extends Base{
    /**
     * 从区块交易记录表(txblock) 导出区块信息存入区块表(height_blocks)
     * @频率: 每2分钟执行一次
     */
    public function get_blocks(){
        $height_blocks_mdl = new HeightBlocksModel();
        $txblock_mdl = new TxblockModel();
        //取出最新存入的那一个区块 $height_blocks的最后一个元素
        $end_block = $height_blocks_mdl->getEndBlock();
        if(!$end_block) $end_block = 0;
        //获取未存储的区块信息
        $data = $txblock_mdl->getDataForHeightBlocks($end_block);
        //向区块表插入信息
        if(!empty($data)){
            $result = $height_blocks_mdl->insertMany($data);
        }
    }

    /**
     * 未出奖的区块随机出奖
     * @频率: 每2分钟执行一次
     */
    public function block_reward(){
        $height_blocks_mdl = new HeightBlocksModel();
        $joy_item_mdl = new JoyItemModel();
        $msg_mdl = new MsgModel();
        $members_joy_mdl = new MembersJoyModel();
        $member_team_mdl = new MemberTeamModel();
        //取出所有未出奖的区块
        $height_blocks_norewarded = $height_blocks_mdl->getUnrewardedBlocks();
        //出奖
        foreach($height_blocks_norewarded as $k => $v){
            $txid = $this->doBlockReward($v);
            if($txid){
                //发送ela奖牌
                $user_id = $joy_item_mdl->getUseridByTxId($txid);
                $result_ela = $members_joy_mdl->add_medal_num($user_id, 1);
                //发送team奖牌
                $team_id = $joy_item_mdl->getTeamidByTxId($txid);
                if($team_id > 0){
                    $result_team = $member_team_mdl->add_medal_num($user_id, $team_id, 1);
                }
                //更新发送状态
                if($result_ela){
                    $result = $joy_item_mdl->editOneJoyItemByWhere(['txId' => $txid], ['status' => 1]);
                }
                if(isset($result) && $result !== false){
                    //出奖成功更新区块表记录
                    $height_blocks_mdl->editOneByWhere(['height' => $v], ['rewarded' => 1]);
                    //出奖成功插入消息
                    $data = [];
                    $data['type'] = 1;
                    $data['content'] = config('msg_content.1');
                    $data['receive_user_id'] = $user_id;
                    $data['create_time'] = time();
                    $msg_mdl->insertOne($data);
                }
            }
        }
    }

    /**
     * 从区块交易记录中随机取一条出奖
     */
    private  function doBlockReward($height){
        $txblock_mdl = new TxblockModel();
        //取出这个区块下的所有交易txid
        $txids = $txblock_mdl->getAllJoyTxidByHeight($height);
        //以字典升序排序
        sort($txids, SORT_STRING);
        $n = count($txids);
        if($n > 0){
            //块的时间戳对n取模
            $timestamp = $txblock_mdl->get_block_timestamp($height);
            $reward_n = $timestamp % $n;
            //中奖交易的txid
            $reward_txid = $txids[$reward_n];
            return $reward_txid;
        }else{
            return 0;
        }
    }

    /**
     * 每日top10奖励
     * @频率: 每天00:10:00执行一次
     */
    public function get_dailytop(){
        $txblock_mdl = new TxblockModel();
        $joy_mdl = new JoyModel();
        $joy_item_mdl = new JoyItemModel();
        $daily_top_mdl = new DailyTopModel();
        //前一天时间区间
        $begin_timestamp = strtotime(date('Y-m-d 00:00:00', time())) - 24*60*60;
        $end_timestamp = strtotime(date('Y-m-d 23:59:59', time())) - 24*60*60;
        //在这个时间区间内所有txid记录
        $txids = $txblock_mdl->getAllTxidBetweenTime($begin_timestamp, $end_timestamp);
        //支持发奖励的joy_id
        $joy_ids = $joy_mdl->getAllJoyidWhereisAward();
        //得分最高的记录
        $tops = $joy_item_mdl->get_topscore($joy_ids, $txids, 10);
        //把top10数据插入daily_top表
        $data = [];
        foreach($tops as $k => $v){
            //判断是否插入过了
            $check = $daily_top_mdl->getOneByTimestampUserid($begin_timestamp, $v['user_id']);
            if(!$check){
                $data[$k]['user_id'] = $v['user_id'];
                $data[$k]['team_id'] = $v['team_id'];
                $data[$k]['joy_id'] = $v['joy_id'];
                $data[$k]['total_score'] = $v['total_score'];
                $data[$k]['cur_time'] = $begin_timestamp;
                $data[$k]['create_time'] = time();
            }
        }
        if($data){
            $result = $daily_top_mdl->insertMany($data);
        }
        //发送奖励
        // $this->reward_daily_top();
        //发送消息
        // $this->msg_daily_top();
    }

    /**
     * 给每日top10会员发送5块奖牌的奖励
     */
    private function reward_daily_top(){
        $daily_top_mdl = new DailyTopModel();
        $member_team_mdl = new MemberTeamModel();
        $members_joy_mdl = new MembersJoyModel();
        $unrewards = $daily_top_mdl->get_allUnReward();
        //发送奖励
        if($unrewards){
            foreach($unrewards as $k => $v){
                //ela发送奖励
                $result = $members_joy_mdl->add_medal_num($v['user_id'], 5);
                //如果加入了战队 战队发送奖励
                if($v['team_id'] > 0){
                    $member_team_mdl->add_medal_num($v['user_id'], $v['team_id'], 5);
                }
                if($result){
                    //发送奖励成功 更改发送状态
                    $result_issue = $daily_top_mdl->editByWhere(['id' => $v['id']], ['is_issue' => 1]);
                }
            }
        }
    }

    /**
     * 给已发送奖励未通知消息的记录通知消息
     */
    private function msg_daily_top(){
        $daily_top_mdl = new DailyTopModel();
        $msg_mdl = new MsgModel();
        $unnoticed = $daily_top_mdl->get_allUnNoticed();
        //发送消息
        if($unnoticed){
            foreach($unnoticed as $k => $v){
                $data = [];
                $data['type'] = 2;
                $data['content'] = config('msg_content.2');
                $data['receive_user_id'] = $v['user_id'];
                $data['create_time'] = time();
                $result = $msg_mdl->insertOne($data);
                if($result){
                    //发送消息成功 更改发送状态
                    $result_issue = $daily_top_mdl->editByWhere(['id' => $v['id']], ['is_notice' => 1]);
                }
            }
        }
    }
    
    /**
     * 确认手续费的发送情况 并消息通知用户
     * @频率: 每2分钟执行一次
     */
    public function get_charge_status(){
        $member_charge_mdl = new MemberChargeModel();
        $transfer_mdl = new TransferModel();
        $msg_mdl = new MsgModel();
        //从member_charge表中找出所有 已在transfer创建但不知是否已发送手续费的记录
        $un_issues = $member_charge_mdl->get_AllUnissue();
        //去transfer表查询手续费是否已发送
        foreach($un_issues as $k => $v){
            $status = $transfer_mdl->getStatusById($v['transfer_id']);
            //未发送不做处理
            //已发送更新状态
            if($status){
                $update = [];
                $update['is_issue'] = 1;
                $update['update_time'] = time();
                $result_charge = $member_charge_mdl->updateOneById($v['id'], $update);
                //更新成功发送消息
                if($result_charge){
                    $data = [];
                    $data['type'] = 3;
                    $data['content'] = config('msg_content.3');
                    $data['receive_user_id'] = $v['user_id'];
                    $data['create_time'] = time();
                    $result_msg = $msg_mdl->insertOne($data);
                    //发送消息成功 更改状态
                    if($result_msg){
                        $update_charge = [];
                        $update_charge['is_notice'] = 1;
                        $update_charge['update_time'] = time();
                        $member_charge_mdl->updateOneById($v['id'], $update_charge);
                    }
                }
            }
        }
    }
    
}
