<?php
namespace app\mobile\controller;

use app\mobile\model\TxblockModel;
use app\mobile\model\MembersModel;
use app\mobile\model\MemberTeamModel;
use app\mobile\model\TeamsModel;
use app\mobile\model\DailyTopModel;
use app\mobile\model\HeightBlocksModel;
use app\mobile\model\JoyItemModel;
use app\mobile\model\MembersJoyModel;
use app\mobile\model\JoyModel;

class Block extends Base{

    public function index(){
        return $this->fetch();
    }

    public function ranking_list(){
        $cur_tab = input('param.cur_tab') ? input('param.cur_tab'): 'dailytop';

        $this->assign([
            'tabs' => $this->getTabs(),
            'cur_tab' => $cur_tab,
            'type' => 'pullon',
        ]);
        return $this->fetch(); 
    }

    public function ajax_getRankingList(){
        $cur_tab = input('param.type') ? input('param.type'): 'dailytop';
        $searchText = input('param.searchText') ? input('param.searchText'): '';
        $dateText = input('param.dateText') ? input('param.dateText'): '';
        $page = input('param.page') ? input('param.page'): 1;
        $limit = input('param.pageNum') ? input('param.pageNum'): 10;
        $offset = ($page - 1) * $limit;
        
        $where = [];
        if(!empty($searchText)){
            $where['m.nickname'] = ['like', '%' . $searchText . '%'];
        }
        if($cur_tab == 'dailytop' && !empty($dateText)){
            $where['dt.create_time'] = ['between', [strtotime($dateText.' 00:00:00'), strtotime($dateText.'23:59:59')]];
            $where['begin_timestamp'] = strtotime($dateText.' 00:00:00');
            $where['end_timestamp'] = strtotime($dateText.'23:59:59');
        }

        if($cur_tab  == 'dailytop' && $page > 1){
            return json([]);
        }
        
        $result = $this->getRanking($cur_tab, $where, $offset, $limit);
        foreach($result as $key => $item){
            $result[$key]['type'] = $cur_tab;
        }

        return json($result);
    }

    private function getTabs(){
        return [
            'dailytop' => [
                'name' => '每日TOP10排行榜',
                'href' => url('block/ranking_list', ['tab' => 'dailytop']),
            ],
            // 'total' => [
            //     'name' => '奖牌榜',
            //     'href' => url('block/ranking_list', ['tab' => 'total']),
            // ]
        ];
    }

    private function getRanking($cur_tab='dailytop', $where=[], $offset=0, $limit=10){
        switch ($cur_tab) {
            case 'dailytop':
                if(!isset($where['dt.create_time'])){
                    $where['dt.create_time'] = ['between', [strtotime(date('Y-m-d 00:00:00', time())), strtotime(date('Y-m-d 23:59:59', time()))]];
                    $begin_timestamp = strtotime(date('Y-m-d 00:00:00', time()));
                    $end_timestamp = strtotime(date('Y-m-d 23:59:59', time()));
                }else{
                    $begin_timestamp = $where['begin_timestamp'];
                    $end_timestamp = $where['end_timestamp'];
                    unset($where['begin_timestamp']);
                    unset($where['end_timestamp']);
                }
                // $where['dt.txid'] = ['neq', ''];
                // $dt_mdl = new DailyTopModel();
                // $result = $dt_mdl->getDailyTopInfoByWhere($where, $offset, $limit);
               
                $txblock_mdl = new TxblockModel();
                $txids = $txblock_mdl->getAllTxidBetweenTime($begin_timestamp, $end_timestamp);
                $joy_mdl = new JoyModel();
                $joy_ids = $joy_mdl->getAllJoyidWhereisAward();
                $member_mdl = new MembersModel();
                $mwhere = [];
                if(isset($where['m.nickname'])){
                    $mwhere['nickname'] = $where['m.nickname'];
                }
                $user_ids = $member_mdl->getUseridByWhere($mwhere);
                $ji_mdl = new JoyItemModel();
                $result = $ji_mdl->getDailyTopInfo($joy_ids, $txids, $limit, $user_ids);
                break;
            case 'total':
                $where['m.status'] = 1;
                $mj_mdl = new MembersJoyModel();
                $result = $mj_mdl->getTotalByWhere($where, $offset, $limit);
                break;
        }
        
        return $result;
    }

    public function ranking_detail(){
        $user_id = input('param.user_id') ? input('param.user_id'): session('user_id');

        $this->assign([
            'user_id' => $user_id,
            'type' => 'pullon',
        ]);
        return $this->fetch(); 
    }

    public function ajax_getRankingDetail(){
        $user_id = input('param.user_id') ? input('param.user_id'): session('user_id');
        $page = input('param.page') ? input('param.page'): 1;
        $limit = input('param.pageNum') ? input('param.pageNum'): 10;
        $offset = ($page - 1) * $limit;
        
        $where = [
            'ji.user_id' => $user_id,
            'ji.txId' => ['neq', ''],
            'ji.status' => 1,
        ];
        $joyItem_mdl = new JoyItemModel();
        $result = $joyItem_mdl->getTxblockDetailByWhere($where, $offset, $limit);
        foreach($result as $key => $item){
            $result[$key]['create_time'] = date('Y/m/d H:i:s', $item['create_time']);
        }

        return json($result);
    }

    /**
     * 生态排行榜页面
     */
    public function ranking_teams(){
        $teams_mdl = new TeamsModel();
        $members_joy_mdl = new MembersJoyModel();
        //获取所有战队id和名称
        $teams = $teams_mdl->getAllTeams();
        foreach($teams as $k => $v){
            $teams[$k]['members_num'] = $members_joy_mdl->calculateMembersNumByTeamid($v['id']);
            $teams[$k]['medal_num'] = $members_joy_mdl->getTotalMedalNumByTeamid($v['id']);
        }

        $this->assign([
            'teams' => $teams,
            'teams_json' => json_encode($teams)
        ]);
        return $this->fetch();
    }

    /**
     * 获取生态排行榜动态数据
     */
    public function ajax_getRankingTeams(){
        $member_team_mdl = new MemberTeamModel();
        $members_mdl = new MembersModel();
        $teams_mdl = new TeamsModel();
        $members_joy_mdl = new MembersJoyModel();

        $team_id = input('param.team_id');
        $keyword = input('param.keyword');
        $page = input('param.page') ? input('param.page') : 1;

        $data = $members_joy_mdl->getAllOrderbyMedalnumByTeamid($team_id, $keyword, 10, $page);
        $data['hasNext'] = $data['current_page'] < $data['last_page'] ? true : false;

        //计算用户当前应得的ela和eco数量
        $total_ela_medal_num = $members_joy_mdl->getTotalMedalNum();
        $base_ela = $members_mdl->getBaseEla($total_ela_medal_num);
        $total_eco_medal_num = $members_joy_mdl->getTotalMedalNumByTeamid($team_id);
        $total_eco_currency = $teams_mdl->getTotalecocurrencyByTeamid($team_id);
        $base_eco = $member_team_mdl->getBaseEco($total_eco_medal_num, $total_eco_currency);
        foreach($data['data'] as $k => $v){
            $data['data'][$k]['ela'] = number_format($v['medal_num'] * $base_ela, 6);
            $data['data'][$k]['eco'] = number_format($v['medal_num'] * $base_eco, 6);
        }

        return json($data);
    }

    /**
     * 所有区块页面
     */
    public function block_records(){
        return $this->fetch();
    }

    /**
     * 获取区块动态数据
     */
    public function ajax_getBlockRecords(){
        $height_blocks_mdl = new HeightBlocksModel();

        $page = input('param.page') ? input('param.page') : 1;
        $data = $height_blocks_mdl->getAllBlocks(20, $page);
        $data['hasNext'] = $data['current_page'] < $data['last_page'] ? true : false;

        return json($data);
    }

    /**
     * 出块记录详情页面
     */
    public function block_record_detail(){
        $height_blocks_mdl = new HeightBlocksModel();

        $height = input('param.height');
        $block = $height_blocks_mdl->getOneBlock($height);
        
        $this->assign([
            'block' => $block
        ]);
        return $this->fetch();
    }

    /**
     * 获取出块记录详情动态数据
     */
    public function ajax_getBlockRecordDetail(){
        $txblock_mdl = new TxblockModel();

        $height = input('param.height');
        $page = input('param.page') ? input('param.page') : 1;
        $data = $txblock_mdl->getAllTransactionsDetailByHeight($height, 20, $page);
        $data['hasNext'] = $data['current_page'] < $data['last_page'] ? true : false;

        return json($data);
    }

}
