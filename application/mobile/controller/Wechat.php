<?php
namespace app\mobile\controller;

use think\Controller;
use app\mobile\model\MembersModel;
use app\mobile\controller\Member as MemberController;

class Wechat extends Controller{
    protected $AppID;
    protected $AppSecret;

    public function __construct(){
        $this->AppID = config('wechat.AppID');
        $this->AppSecret = config('wechat.AppSecret');
    }

    /**
     * 触发请求用户授权
     */
    public function get_code(){
        $get_code_redirect_url = urlencode(config('wechat.get_code_redirect_url'));
        $get_code_snsapi_userinfo_url = sprintf(config('wechat.get_code_snsapi_userinfo_url'), $this->AppID, $get_code_redirect_url, 'state');
        header("Location:$get_code_snsapi_userinfo_url");
    }

    /**
     * 用户同意授权后回调方法，此方法名在config注册为wechat.get_code_redirect_url
     */
    public function get_code_redirect(){
        $code = input('get.code');
        $state = input('get.state');
        if(!$code){
            //获取code失败
            echo 'failed to get code';exit;
        }

        // 根据code获取网页授权access_token openid
        $access_json = $this->get_oauth2_access_token($code);
        $access_arr = json_decode($access_json, true);
        if(isset($access_arr['errcode'])){
            //获取access_token失败
            echo '<pre>';
            print_r($access_arr);exit;
        }

        // 根据access_token和openid获取用户信息
        $userinfo_json = $this->get_userinfo($access_arr['access_token'], $access_arr['openid']);
        $userinfo_arr = json_decode($userinfo_json, true);
        if(isset($userinfo_arr['errcode'])){
            //获取用户信息失败
            echo '<pre>';
            print_r($userinfo_arr);exit;
        }
        
        // 获取到用户信息后，与用户表进行交互
        $deal_result = $this->deal_userinfo($userinfo_arr);
        if(!$deal_result){
            //创建或更新用户失败
        }

        // 存储登录状态到session
        $session_data = $this->get_login_session_data($userinfo_arr['openid']);
        session('userid', $session_data['user_id']);
        session('openid', $session_data['openid']);
        session('nickname', $session_data['nickname']);
        session('ident_code', $session_data['ident_code']);

        // 登录成功跳转到主页
        $this->redirect(url('mobile/index/index'));
    }

    /**
     * 根据code获取网页授权access_token
     */
    private function get_oauth2_access_token($code){
        $get_oauth2_access_token_url = sprintf(config('wechat.get_oauth2_access_token_url'), $this->AppID, $this->AppSecret, $code);
        return curl_get_https($get_oauth2_access_token_url);
    }

    /**
     * 根据access_token和openid获取用户信息
     */
    private function get_userinfo($access_token, $openid){
        $get_userinfo_url = sprintf(config('wechat.get_userinfo_url'), $access_token, $openid, 'zh_CN');
        return curl_get_https($get_userinfo_url);
    }

    /**
     * 获取到最新用户信息后，与members表进行交互
     */
    private function deal_userinfo($userinfo){
        $members_mdl = new MembersModel();
        $exist_userid = $members_mdl->getUseridByOpenid($userinfo['openid']);
        if($exist_userid > 0){
            //用户已存在，则查看nickname、head_img_url、sex是否需要更新
            $exist_userinfo = $members_mdl->getOneMember($exist_userid);
            if($exist_userinfo['nickname'] != $userinfo['nickname'] || $exist_userinfo['sex'] != $userinfo['sex'] || $exist_userinfo['headimgurl'] != $userinfo['headimgurl']){
                $userinfo['id'] = $exist_userid;
                $result = $members_mdl->editMember($userinfo);
            }else{
                $result = true;
            }
        }else{
            //用户不存在，则创建用户
            $member_ctl = new MemberController();
            $userinfo['ident_code'] = $member_ctl->getIdentCode();
            $result = $members_mdl->insertMember($userinfo);
        }
        return $result;
    }

    /**
     * 获取登录后session要存储的数据
     */
    private function get_login_session_data($openid){
        $members_mdl = new MembersModel();
        return $members_mdl->getOneMemberByOpenid($openid);
    }


}