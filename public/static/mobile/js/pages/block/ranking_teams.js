var app = new Framework7({root:'#app'});
var $$ = Dom7;

//当前战队id
var curr_team_id = 1;

//每个战队的当前页
var team_page1 = 0;
var team_page2 = 0;
var team_page3 = 0;
var team_page4 = 0;

//当前tab页是否允许滚动
var allow_infinite1 = true;
var allow_infinite2 = true;
var allow_infinite3 = true;
var allow_infinite4 = true;

//获取当前页面信息
function get_curr(){
    var curr = {};
    curr.team_id = curr_team_id;
    switch(curr.team_id){
        case 1:
            curr.team_page = team_page1;
            curr.allow_infinite = allow_infinite1;
            curr.ul_obj = $$('#list-ul1');
            break;
        case 2:
            curr.team_page = team_page2;
            curr.allow_infinite = allow_infinite2;
            curr.ul_obj = $$('#list-ul2');
            break;
        case 3:
            curr.team_page = team_page3;
            curr.allow_infinite = allow_infinite3;
            curr.ul_obj = $$('#list-ul3');
            break;
        case 4:
            curr.team_page = team_page4;
            curr.allow_infinite = allow_infinite4;
            curr.ul_obj = $$('#list-ul4');
            break;
    }
    return curr;
}

//上拉加载
$$('.infinite-scroll-content').on('infinite', function () {
    switch(curr_team_id){
        case 1:
            if(!allow_infinite1) return;
            allow_infinite1 = false;
            break;
        case 2:
            if(!allow_infinite2) return;
            allow_infinite2 = false;
            break;
        case 3:
            if(!allow_infinite3) return;
            allow_infinite3 = false;
            break;
        case 4:
            if(!allow_infinite4) return;
            allow_infinite4 = false;
            break;
    }
    ajax_getRankingTeams('up');
})

//下拉刷新
$$('.ptr-content').on('ptr:refresh', function (e) {
  console.dir(e)
});



$$('.tab-link').on('click', function(){
    curr_team_id = parseInt($$(this).attr('data-teamid'));
    for(var i = 0; i < teams.length; i++){
        if(teams[i].id == curr_team_id){
            var curr_team = teams[i];
        }
    }

    //战队静态数据更新
    $$('#team_statistical_name').text(curr_team.name + '战队');
    $$('#team_statistical_data').text('总计：'+ curr_team.members_num + '人  |  奖牌数：' + curr_team.medal_num + '个');
});


//动态获取数据
function ajax_getRankingTeams(type){
    switch(curr_team_id){
        case 1:
            var page = type == 'up' ? team_page1 + 1 : 1;
            $$('#tab1').append(
                `<div class="preloader infinite-scroll-preloader"></div>`
            );
            break;
        case 2:
            var page = type == 'up' ? team_page2 + 1 : 1;
            break;
        case 3:
            var page = type == 'up' ? team_page3 + 1 : 1;
            break;
        case 4:
            var page = type == 'up' ? team_page4 + 1 : 1;
            break;
    }
    var data = {
        'team_id':curr_team_id,
        'page':page
    };
    app.request.json(getRankingTeams_url, data, function(res){
        switch(curr_team_id){
            case 1:
                allow_infinite1 = true;
                var ul_obj = $$('#list-ul1');
                break;
            case 2:
                allow_infinite2 = true;
                var ul_obj = $$('#list-ul2');
                break;
            case 3:
                allow_infinite3 = true;
                var ul_obj = $$('#list-ul3');
                break;
            case 4:
                allow_infinite4 = true;
                var ul_obj = $$('#list-ul4');
                break;
        }
        // if(!res.hasNext){
        //     app.infiniteScroll.destroy('.infinite-scroll-content');
        //     $$('.infinite-scroll-preloader').remove();
        //     return;
        // }
        $$('.infinite-scroll-preloader').remove();
        var listhtml = getListHtml(res.data);
        ul_obj.append(listhtml);
    })
}

//列表填充数据
function getListHtml(data){
    var html = '';
    for(var i = 0; i < data.length; i++){
        html += 
        `<li>
            <div class="card integral_card">
                <div class="integral_box">
                    <div class="integral_photo"><span class="integral_serial">`+3+`</span><img src="`+data[i].headimgurl+`" width="80" height="80"/></div>
                    <div class="integral_data">
                        <div class="user_name">`+data[i].nickname+`</div>
                        <div class="user_medal">奖牌：<span>`+data[i].eco_medal_num+`</span></div>
                        <div class="user_ela">ELA币：<span>`+data[i].ela+`</span></div>
                        <div class="user_ecology">生态币：<span>`+data[i].eco+`</span></div>
                    </div>
                </div>
            </div>
        </li>`;
    }
    return html;
}

// create searchbar
var searchbar = app.searchbar.create({
    el: '.searchbar',
    searchContainer: '.list',
    searchIn: '.item-title',
    on: {
        search(sb, query, previousQuery) {
            console.log(query, previousQuery);
        }
    }
});