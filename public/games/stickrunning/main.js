var gameConfig = {
	gameId: "61f143e520804499919733156c98d169",
	userId: "4D59634F-BFF0-4634-8D5E-E91B1FCAA1AE-s1",
	activeRotator: true,
	gameIsPortrait: false,
	AdsWaitForInput: true,
	debugMode: false
};
window["projectManage"] = new ProjectManage();

function onVisibilityChanged() {
	if (document.hidden || document.mozHidden || document.webkitHidden || document.msHidden){
		if(typeof cr_setSuspended !== "undefined") cr_setSuspended(true);
	} else{
		if(typeof cr_setSuspended !== "undefined") cr_setSuspended(false);
	}
		
};

document.addEventListener("visibilitychange", onVisibilityChanged, false);
document.addEventListener("mozvisibilitychange", onVisibilityChanged, false);
document.addEventListener("webkitvisibilitychange", onVisibilityChanged, false);
document.addEventListener("msvisibilitychange", onVisibilityChanged, false);

function onTouchEvent(){
	if($("#preroll_play_bg, #preroll_play_over_bg, #preroll_play_frame").length){
		$("#preroll_play_bg, #preroll_play_over_bg, #preroll_play_frame").remove();
		if(typeof cr_setSuspended === "function") cr_setSuspended(false);
		$("#c2canvas").show();
	}
}

document.addEventListener("mouseup", onTouchEvent, false);
document.addEventListener("touchend", onTouchEvent, false);

function c2LayoutChange(state,name,force){
	// console.log(state,name,force);
}

//ROTATOR START
var onMobile, reallyOnMobile; onMobile = reallyOnMobile = (navigator.userAgent.match(/(mobile|android|iphone|ipad|blackberry|symbian|symbianos|symbos|netfront|model-orange|javaplatform|iemobile|windows phone|samsung|htc|opera mobile|opera mobi|opera mini|presto|huawei|blazer|bolt|doris|fennec|gobrowser|iris|maemo browser|mib|cldc|minimo|semc-browser|skyfire|teashark|teleca|uzard|uzardweb|meego|nokia|bb10|playbook)/gi));
function checkOrientation(){
	if(!gameConfig.activeRotator || !reallyOnMobile){
		hideRotator();
		return true;
	}
	if(gameConfig.gameIsPortrait && jQuery(window).width() > jQuery(window).height()){
		displayRotator('portrait');
		return false;
	}
	if(!gameConfig.gameIsPortrait && jQuery(window).width() < jQuery(window).height()){
		displayRotator('landscape');
		return false;
	}
	hideRotator();
	return true;
}

function displayRotator(orientation){
	var gameElement = document.getElementById("c2canvasdiv");
	var rotatorElement = document.getElementById("rotator");
	if(rotatorElement.style.display != "none") return false;
	if(typeof cr_setSuspended === "function") cr_setSuspended(true);
	gameElement.style.display = "none";
	rotatorElement.innerHTML = "";
	rotatorElement.innerHTML = '<img id="rotatorLogo"  style="width:100%;" src="./rotate-device-to-' + orientation + '.png" />';
	rotatorElement.style.display = "block";
	rotatorElement.style.backgroundColor = "white";
	rotatorElement.style.width = "100%";
    rotatorElement.style.height = "100%";
    rotatorElement.style.position = "absolute";
	window.centerRotatorTimer = setInterval(function(){	centerRotator();	}, 100);
	return true;
}

function centerRotator(){
	var rotatorElement = document.getElementById("rotator");
	rotatorElement.style.paddingLeft = parseInt(jQuery(window).width() / 2 - jQuery("#rotatorLogo").width() / 2)+"px";
	rotatorElement.style.paddingTop = parseInt(jQuery(window).height() / 2 - jQuery("#rotatorLogo").height() / 2) +"px";
	rotatorElement.style.paddingBottom =  parseInt(jQuery(window).height() / 2 - jQuery("#rotatorLogo").height() / 2) +"px";
}

function hideRotator(){
	var gameElement = document.getElementById("c2canvasdiv");
	var rotatorElement = document.getElementById("rotator");
	if(rotatorElement == null) return false;
	if(rotatorElement.style.display == "none") return false;
	rotatorElement.innerHTML = "";
	rotatorElement.style.display = "none";
    gameElement.style.display = "block";
    window.projectManage && window.projectManage.resize(); 
	cr_setSuspended(false);
	clearInterval(window.centerRotatorTimer);
	return true;
}

var waitForJQ = setInterval(function(){
		if(typeof jQuery === "undefined") return;
		jQuery(document).ready(function (){
			if(checkOrientation() || !gameConfig.activeRotator)	hideRotator();
			if(gameConfig.activeRotator && reallyOnMobile){
				jQuery(window).resize(function(){
                    if(checkOrientation())	hideRotator();
				});
            }   
		});
        clearInterval(waitForJQ);
	},100);
//ROTATOR END
function loadGame(){			
	if(typeof cr_createRuntime === "function") cr_createRuntime("c2canvas");
	else setTimeout(loadGame, 500);
}

jQuery(document).ready(function () {
	loadGame();

	var dom = document.getElementsByClassName("main")[0];
	dom.addEventListener('touchmove', function(e){
		e.preventDefault();
		// e.stopPropagation();
	}, false);
})