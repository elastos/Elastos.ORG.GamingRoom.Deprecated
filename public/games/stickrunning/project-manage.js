var ProjectManage = (function() {
    var BASE_WIDTH = 568;
    var BASE_HEIGHT = 320; 
    var STORAGE_KEY = 'stickmanSchoolRun_YILAIYUN';

    var projectManage = function () {

        this.scale = 1
        this.offestX = 0;
        this.currScale = null;
        this.startTime = this.getTime()

        this.currView = null;
        this.tempView = null;
        this.toastView = null;

        this.isOver = false;
        this.isToast = false;
        this.isUpload = false;


        this.getScaleAndOffestX();
        this.setScale();
        this.EventsListener();
    }
    projectManage.prototype.resize = function () {
        this.getScaleAndOffestX();
        this.setScale();
    } 

    projectManage.prototype.getScaleAndOffestX = function () {

        var width = window.innerWidth;
        var height = window.innerHeight;

        var scaleX = width/BASE_WIDTH;
        var scaleY = height/BASE_HEIGHT;
        this.scale = scaleX > scaleY ? scaleY : scaleX;

        // if(width * BASE_HEIGHT / BASE_WIDTH > height) {
        //     this.scale = height / BASE_HEIGHT;
        // } else {
        //     this.scale = width / BASE_WIDTH;
        // }

        var nornalWidth = BASE_WIDTH * this.scale;
    
        this.offestX =  width - nornalWidth;

    }

    projectManage.prototype.setScale = function () {
        if(this.scale && this.currScale !=  this.scale){
            this.currScale = this.scale;
            $('.view-content').css({
                "transform":"scale("+this.scale+")",
            });
        }
    }

    projectManage.prototype.EventsListener = function () {

        $(".exit-btn").on("touchstart",function (ev) {
            cr_setSuspended(true);
            ev.stopPropagation();
            ev.preventDefault();
            this.showToast("toast-exit");
            return false;
        }.bind(this));
        //
        $('.upload-btn').on("touchstart",function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            this.showToast("toast-upload");
        }.bind(this));
        
        //上传成绩
        $('.upload-click').on("touchstart",function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            this.uploadScroe();
            return false;
        }.bind(this));

        // 关闭toast
        $(".close").on("touchstart",function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            this.hideToast();  
            return false; 
        }.bind(this)); 

        //退出游戏
        $(".exit-click").on("touchstart",function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            this.cleanSaveData();
            history.back(-1);
            return false;
        }.bind(this));

        //重新开始游戏
        $(".restart-btn").on("touchstart",function(ev) {
            ev.stopPropagation();
            ev.preventDefault();
            this.cleanSaveData();
            window.location.reload();
            return false;
        }.bind(this));

    }

    projectManage.prototype.screenMange = function (name_, params_) {
        if(this.isToast) return;
        // console.log(name_, params_.toString());
        if (name_ === "pubWebView" && params_.toString() == '') {
            this.tempView = null;
        }

        if(name_ === 'allowAccess2NextLvl') {
            this.isOver = true;
            this.tempView = 'success';
        }
        if(name_ === "launchPub" && !this.isOver) {
            this.tempView = 'fail';
        }

    
        if (name_ === "pause") {
            this.tempView  = "paused";
        }

        this.showView(this.tempView);
    }

    projectManage.prototype.showView = function (view) {
        this.show(view, this.currView);
        this.currView = view;
    }
    projectManage.prototype.hideView = function () {
        if(this.currView) {
            this.hide(this.currView);
            this.isOver = false;
            this.currView = null;
        }
    }

    projectManage.prototype.showToast = function (view) {
        if(this.isToast) return;
        this.show(view, this.toastView);
        this.toastView = view;
        cr_setSuspended(true);
        this.isToast = true;
        $("#toast-view").show();
    }
    projectManage.prototype.hideToast = function (callback) {
        if(this.toastView) {
            $("#toast-view").hide();
            this.hide(this.toastView);
            cr_setSuspended(false);
            this.toastView = null;
            setTimeout(function () {
                this.isToast = false;
                callback && callback();
            }.bind(this), 0);
        }
    }

    projectManage.prototype.showLoaingState = function () {
        $('#loading').show();
    }
    projectManage.prototype.hideLoadingState = function() {
        $('#loading').hide();
    }

    projectManage.prototype.show = function (view, saveView) {
        if(!view) {
            this.hideView(saveView);
            return;
        }
        if(saveView) {
            if(saveView === view) {
                return;
            } else {
                this.hideView(saveView);
            }
        }
        $("."+view).show();
    }

    projectManage.prototype.hide = function (view) {
        $("."+view).hide();
    } 

    projectManage.prototype.uploadScroe = function () {
        var data = this.getParams();
        var gameDate = this.getGameData();
        console.log(gameDate);
        // if(params == null) return;
        var url = data.serverurl;   
        // delete data.serverurl;
        data['starttime'] = this.startTime;
        var endtime = this.getTime();
        data['endtime'] = endtime;
        data['time'] = endtime - this.startTime;
        // data['level'] = gameDate['level'];
        data['score'] = gameDate['score'];
        data['joy_id'] = 2;

        var params = {
            source: 'joy',
            data: {
                'uid':data.uid, 
                'time':data.time, 
                'score':data.score,
                'starttime':data.starttime,
                'endtime':data.endtime,
                'joy_id':data.joy_id
            }
        }

        console.log(params)
        this.showLoaingState();
        this.postMessage(url, params, function(isSuccess) {
            this.hideToast(function() {
                this.hideLoadingState();
                if(isSuccess) {
                    this.showToast('upload-success');
                } else {
                    this.showToast('upload-fail');
                }
            }.bind(this));
        }.bind(this));
    
    }
    projectManage.prototype.cleanSaveData = function () {
        localStorage.removeItem(STORAGE_KEY);
    }
    projectManage.prototype.getSaveData = function () {
        return localStorage.getItem(STORAGE_KEY);
    }
    projectManage.prototype.setSaveData  = function (data) {
        localStorage.setItem(STORAGE_KEY, data)
    }

    projectManage.prototype.getParams = function () {
        var obj = {};
        var url = window.location.href;
        if(url.indexOf('?') > 0){
            var paramStr = url.split('?')[1];
            var paramArr = paramStr.split("&");
            var i = 0, len = paramArr.length;
            for(i; i<len; i++){
                var arr = paramArr[i].split("=");
                obj[arr[0]] = arr[1];
            }
        }
        return obj;
    }

    projectManage.prototype.getGameData = function () {
        var data = this.formatGameData(localStorage.getItem(STORAGE_KEY));
        return data;
    }
    projectManage.prototype.formatGameData = function(str) {
        var data = {
            star: 0,
            score: 0,
            level: 0
        }
        if(str === null) {
            return data;
        }
        var strArr = str.split('a-1s0');
        var levelStr= strArr[0];
        data['level'] = levelStr.slice(0,levelStr.length-1);
        levelStr = strArr[1];
        strArr = levelStr.split('a');
        strArr.shift();
        data['star'] = 0;
        
        var i = 0; len = strArr.length;
        for(i; i < len; i++){
            var item = strArr[i].split('s');       
            data["score"] += parseInt(item[0]);     
            data["star"] += parseInt(item[1]);
        }
        return data;
    }
    projectManage.prototype.postMessage = function (url, data, callback) {
        var saveData = this.getSaveData();
        this.cleanSaveData();
        var _this = this;
        
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function(data) {
                var result = JSON.parse(data)
                if(result.status == 'success'){
                    location.href = result.url;
                }else{
                    console.dir(result)
                }
            },
            error: function (data) {
                console.log("error", data);
                _this.setSaveData(saveData);
                callback(false);
            } 
        });
    }

    projectManage.prototype.getTime = function() {
        console.log(Date.now())
        return parseInt(Date.now()/1000);
    }

    return projectManage;

})();
