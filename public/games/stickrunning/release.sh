rm -rf release/
mkdir release

cp -rf css/ release/css
cp -rf images/ release/images
cp -rf media/ release/media

cp -rf loading-logo.png release/loading-logo.png
cp -rf rotate-device-to-landscape.png release/rotate-device-to-landscape.png

cp -rf levelconfig.csv release/levelconfig.csv
cp -rf starsunlocked.csv release/starsunlocked.csv

cp -rf data.js release/data.js

cp release.html release/index.html

uglifyjs c2runtime.js jquery-2.1.1.min.js project-manage.js main.js -o release/game.js -c -m --screw-ie8
