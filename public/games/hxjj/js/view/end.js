var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var end = /** @class */ (function (_super) {
        __extends(end, _super);
        function end() {
            var _this = _super.call(this) || this;
            _this.RevivePrice = 0;
            _this.has_send = false;
            _this.bg = new Laya.Sprite();
            _this.bg.graphics.drawRect(0, 0, game.width, game.height, "#000000");
            _this.bg.cacheAs = "bitmap";
            _this.bg.alpha = 0.5;
            _this.bg.zOrder = 1;
            _this.addChild(_this.bg);
            _this.content.pivot(_this.content.width * 0.5, _this.content.height * 0.5);
            _this.content.y = game.height * 0.5;
            _this.content.x = game.width * 0.5;
            _this.content.zOrder = 2;
            _this.txt_score.text = game.bt.distance.toString() + '米';
            //this.txt_tips.text = '消耗医疗包x' + this.RevivePrice.toString();
            var best = Laya.LocalStorage.getItem("score");
            if (best == null) {
                best = "0";
            }
            var best_num = parseInt(best.toString());
            if (game.bt.distance > best_num) {
                best_num = game.bt.distance;
                Laya.LocalStorage.setItem("score", best_num.toString());
            }
            _this.txt_best.text = '最佳:' + best_num + '米';
            _this.btn_restart.on(Laya.Event.MOUSE_DOWN, _this, _this.Restart);
            _this.btn_restart2.on(Laya.Event.MOUSE_DOWN, _this, _this.Restart2);
            _this.btn_start.on(Laya.Event.MOUSE_DOWN, _this, _this.GotoMain);
            if (game.has_audio) {
                Laya.SoundManager.stopAll();
                Laya.SoundManager.playSound("res/sound/lose.mp3");
            }
            return _this;
        }
        //提交成绩
        end.prototype.Restart = function () {
            if (game.bt != null) {
                this.has_send = true;
                this.SendData();
            }
        };
        //重新开始游戏没有新手引导
        end.prototype.Restart2 = function () {
            if (this.has_send) {
                return;
            }
            if (game.bt != null) {
                game.bt.RelayOver();
                this.destroy();
                game.ClearAll();
                game.RunBt(true);
            }
        };
        //返回主页
        end.prototype.GotoMain = function () {
            if (this.has_send) {
                return;
            }
            if (game.bt != null) {
                game.bt.RelayOver();
                this.destroy();
                game.ClearAll();
                game.Star();
            }
        };
        //测试数据传输
        end.prototype.SendData = function () {
            console.log("game.url:" + game.url);
            /////////////////测试 by forrest//////////////
            // game.url = 'https://game-dev.elastos.org/api/joy/syncJoySubmit';
            if (game.url != null && game.url != '') {
                var end_time = Laya.timer.currTimer;
                var data = {
                    "uid":game.uid,
                    "time":Math.ceil((game.starttime - end_time) * 0.001).toString(),
                    "score":game.bt.distance.toString(),
                    "starttime":Math.ceil(game.starttime* 0.001).toString(),
                    "endtime":Math.ceil(end_time* 0.001).toString(),
                    "joy_id":3
                };
                var send_data = {
                    "source":"joy",
                    "data":data
                };

                var json_data = JSON.stringify(send_data);
                this.http = new Laya.HttpRequest(); //new一个HttpRequest类
                this.http.once(Laya.Event.PROGRESS, this, this.onProgress); //数据传输中
                this.http.once(Laya.Event.COMPLETE, this, this.onComplete); //数据传输完成后，会返回一个data
                this.http.once(Laya.Event.ERROR, this, this.onError); //数据传输失败后返回
                //post数据的写法
                this.http.send(game.url, 'hxjj='+json_data, 'post', 'text');
            }
        };
        //请求出错
        end.prototype.onError = function (e) {
            console.log("请求出错" + e);
        };
        //请求过程
        end.prototype.onProgress = function (e) {
            console.log("请求进行中" + e);
        };
        //请求完成
        end.prototype.onComplete = function (e) {
            if (game.bt != null) {
                game.bt.RelayOver();
                game.ClearAll();
            }
            var data = JSON.parse(this.http.data);
            if(data.status == 'success'){
                location.href = data.url;
            }
        };
        return end;
    }(ui.endUI));
    view.end = end;
})(view || (view = {}));
//# sourceMappingURL=end.js.map