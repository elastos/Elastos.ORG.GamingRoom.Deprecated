var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**逃脱**/
var view;
(function (view) {
    var login = /** @class */ (function (_super) {
        __extends(login, _super);
        function login() {
            var _this = _super.call(this) || this;
            _this.effects = new Array();
            _this.sp = 5;
            _this.img_index = 0;
            _this.Init();
            return _this;
        }
        login.prototype.Init = function () {
            var bg = new Laya.Sprite();
            bg.loadImage('comp/start_bg.png');
            bg.scale(1, game.height / 960);
            bg.zOrder = 0;
            this.addChild(bg);
            //head
            this.top_img1 = new Laya.Sprite();
            this.top_img1.loadImage('comp/UI_10.png');
            this.top_img1.pivot(this.top_img1.width / 2, -60);
            this.top_img1.pos(game.width / 2, -140);
            this.top_img1.zOrder = 2;
            this.addChild(this.top_img1);
            //开始游戏
            this.btn_ok = new Laya.Sprite();
            this.btn_ok.loadImage('comp/btn_start.png');
            this.btn_ok.pivot(this.btn_ok.width, this.btn_ok.height / 2);
            this.btn_ok.pos(game.width, game.height * 0.5);
            this.btn_ok.zOrder = 2;
            this.btn_ok.on(Laya.Event.MOUSE_DOWN, this, this.GoToBattle);
            this.addChild(this.btn_ok);
            this.btn_ok.on(Laya.Event.MOUSE_DOWN, this, this.GoToBattle);
            this.top1Anim();
            this.CheckAudioBack(true);
            for (var i = 0; i < 10; i++) {
                var effect_img = new Laya.Sprite();
                effect_img.loadImage('effect/starteffect_0' + i + '.png');
                effect_img.pivot(effect_img.width * 0.5, effect_img.height * 0.5);
                effect_img.scale(2.168, 2.168);
                effect_img.pos(game.width - 271, game.height * 0.5);
                effect_img.zOrder = 3;
                this.addChild(effect_img);
                this.effects.push(effect_img);
                effect_img.visible = false;
            }
            for (var i = 10; i <= 23; i++) {
                var effect_img = new Laya.Sprite();
                effect_img.loadImage('effect/starteffect_' + i + '.png');
                effect_img.pivot(effect_img.width * 0.5, effect_img.height * 0.5);
                effect_img.scale(2.168, 2.168);
                effect_img.pos(game.width - 271, game.height * 0.5);
                effect_img.zOrder = 3;
                this.addChild(effect_img);
                this.effects.push(effect_img);
                effect_img.visible = false;
            }
            Laya.timer.loop(50, this, this.UpdateImg);
            this.effects[this.img_index].visible = true;
        };
        login.prototype.UpdateImg = function () {
            this.effects[this.img_index].visible = false;
            this.img_index++;
            if (this.img_index > 23) {
                this.img_index = 0;
            }
            this.effects[this.img_index].visible = true;
        };
        login.prototype.top1Anim = function () {
            Laya.Tween.to(this.top_img1, {
                rotation: -5
            }, 1500, Laya.Ease.linearOut, new Laya.Handler(this, this.top2Anim, null));
        };
        login.prototype.top2Anim = function () {
            Laya.Tween.to(this.top_img1, {
                rotation: 5
            }, 1500, Laya.Ease.linearOut, new Laya.Handler(this, this.top1Anim, null));
        };
        login.prototype.GoToBattle = function () {
            Laya.timer.clearAll(this);
            Laya.stage.removeChild(this);
            this.destroy();
            game.bt.Run(true);
        };
        //设置音效
        login.prototype.SetAudio = function () {
            if (game.has_audio) { //设置为没有声音
                // 更换纹理
                this.btn_audio.graphics.clear();
                var texture = Laya.loader.getRes('comp/button_3.png');
                this.btn_audio.graphics.drawTexture(texture, 0, 0);
                // 设置交互区域
                this.btn_audio.size(texture.width, texture.height);
                game.has_audio = false;
                Laya.LocalStorage.setItem("audio", '0');
                Laya.SoundManager.stopAll();
            }
            else { //设置为有声音
                // 更换纹理
                this.btn_audio.graphics.clear();
                var texture = Laya.loader.getRes('comp/button_2.png');
                this.btn_audio.graphics.drawTexture(texture, 0, 0);
                // 设置交互区域
                this.btn_audio.size(texture.width, texture.height);
                game.has_audio = true;
                Laya.LocalStorage.setItem("audio", "1");
                //Laya.SoundManager.playMusic("res/sound/bgm.mp3", 0);
            }
        };
        login.prototype.random = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        login.prototype.CheckAudioBack = function (ok) {
            if (this.btn_audio == null) {
                game.autoplay = ok;
                if (Laya.LocalStorage.getItem("audio") != '0') {
                    game.has_audio = true;
                    if (!game.autoplay) {
                        game.has_audio = false;
                    }
                }
                else {
                    game.has_audio = false;
                }
                this.btn_audio = new Laya.Sprite();
                if (game.has_audio) {
                    this.btn_audio.loadImage('comp/button_2.png');
                }
                else {
                    this.btn_audio.loadImage('comp/button_3.png');
                }
                this.btn_audio.pivot(this.btn_audio.width / 2, this.btn_audio.height / 2);
                this.btn_audio.pos(this.btn_ok.x - this.btn_ok.width * 0.5, this.btn_ok.y + 160);
                this.btn_audio.zOrder = 2;
                this.btn_audio.on(Laya.Event.MOUSE_DOWN, this, this.SetAudio);
                this.addChild(this.btn_audio);
                if (game.has_audio) {
                    //Laya.SoundManager.playMusic("res/sound/bgm.mp3", 0);
                }
            }
        };
        return login;
    }(ui.loginUI));
    view.login = login;
})(view || (view = {}));
//# sourceMappingURL=login.js.map