var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var battle = /** @class */ (function (_super) {
        __extends(battle, _super);
        function battle() {
            var _this = _super.call(this) || this;
            //==统计数据
            _this.totalScore = 0;
            _this.distance = 0;
            //==配置数据
            _this.items = [
                { id: 1, img: 'UI_8', score: 1 }
            ];
            _this.speeds = [9, 10, 11, 12, 12, 12];
            _this.delays = [2, 1, 0, 0, 0, 0];
            _this.missile_delays = [3, 2, 1, 1, 1, 1];
            _this.air_items = [
                {
                    datas: [{ type: 4, x: 0.5, y: 100, speed_x: 300, speed_y: 200, loop: false }]
                }
            ];
            _this.air_items2 = [
                {
                    datas: [{ type: 1, x: 0.5, y: 100, speed_x: 0, speed_y: 3, loop: false }]
                },
                {
                    datas: [{ type: 3, x: 0, y: 0, speed_x: 10, speed_y: 0, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 1, speed_x: -10, speed_y: 1, loop: true }, { type: 1, x: 0.3, y: 300, speed_x: -10, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 1, y: 0, speed_x: 10, speed_y: 1, loop: true }, { type: 1, x: 0.5, y: 300, speed_x: 10, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 2000, speed_x: 10, speed_y: 2, loop: false }, { type: 1, x: -0.25, y: 400, speed_x: 10, speed_y: 2, loop: false }]
                }
            ];
            _this.air_items3 = [
                {
                    datas: [{ type: 4, x: 0.5, y: 100, speed_x: 300, speed_y: 200, loop: false }]
                },
                {
                    datas: [{ type: 1, x: 0.4, y: 100, speed_x: 0, speed_y: 3, loop: false }, { type: 1, x: 0.6, y: 100, speed_x: 0, speed_y: 3, loop: false }]
                },
                {
                    datas: [{ type: 3, x: 0, y: 0, speed_x: 14, speed_y: 0, loop: true }, { type: 3, x: 0, y: 400, speed_x: 10, speed_y: 0, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 1, speed_x: -14, speed_y: 1, loop: true }, { type: 1, x: 0.3, y: 300, speed_x: -14, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 1, y: 0, speed_x: 14, speed_y: 1, loop: true }, { type: 1, x: 0.5, y: 300, speed_x: 14, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 2000, speed_x: 12, speed_y: 2, loop: false }, { type: 1, x: -0.25, y: 400, speed_x: 12, speed_y: 2, loop: false }]
                }
            ];
            _this.air_items4 = [
                {
                    datas: [{ type: 4, x: 0.5, y: 100, speed_x: 300, speed_y: 200, loop: false }]
                },
                {
                    datas: [{ type: 1, x: 0.25, y: 100, speed_x: 0, speed_y: 3, loop: false }, { type: 1, x: 0.5, y: 100, speed_x: 0, speed_y: 3, loop: false }, { type: 1, x: 0.75, y: 100, speed_x: 0, speed_y: 3, loop: false }]
                },
                {
                    datas: [{ type: 3, x: 0, y: 0, speed_x: 14, speed_y: 0, loop: true }, { type: 3, x: 0, y: 400, speed_x: 10, speed_y: 0, loop: true }, { type: 3, x: 0, y: 800, speed_x: 12, speed_y: 0, loop: true }]
                },
                {
                    datas: [{ type: 3, x: 0, y: 0, speed_x: 14, speed_y: 0, loop: true }, { type: 3, x: 0, y: 400, speed_x: 10, speed_y: 0, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 1, speed_x: -14, speed_y: 1, loop: true }, { type: 1, x: 0.3, y: 300, speed_x: -14, speed_y: 1, loop: true }, { type: 1, x: 0.6, y: 600, speed_x: -14, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 1, y: 0, speed_x: 14, speed_y: 1, loop: true }, { type: 1, x: 0.5, y: 300, speed_x: 14, speed_y: 1, loop: true }, { type: 1, x: 0, y: 600, speed_x: 14, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 2000, speed_x: 12, speed_y: 2, loop: false }, { type: 1, x: -0.25, y: 400, speed_x: 12, speed_y: 2, loop: false }, { type: 1, x: -0.5, y: 600, speed_x: 12, speed_y: 2, loop: false }]
                }
            ];
            _this.air_items5 = [
                {
                    datas: [{ type: 4, x: 0.5, y: 100, speed_x: 300, speed_y: 200, loop: false }]
                },
                {
                    datas: [{ type: 1, x: 0.25, y: 100, speed_x: 0, speed_y: 3, loop: false }, { type: 1, x: 0.5, y: 100, speed_x: 0, speed_y: 3, loop: false }, { type: 1, x: 0.75, y: 100, speed_x: 0, speed_y: 3, loop: false }]
                },
                {
                    datas: [{ type: 3, x: 0, y: 0, speed_x: 14, speed_y: 0, loop: true }, { type: 3, x: 0, y: 400, speed_x: 10, speed_y: 0, loop: true }, { type: 3, x: 0, y: 800, speed_x: 12, speed_y: 0, loop: true }]
                },
                {
                    datas: [{ type: 3, x: 0, y: 0, speed_x: 14, speed_y: 0, loop: true }, { type: 3, x: 0, y: 400, speed_x: 10, speed_y: 0, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 1, speed_x: -14, speed_y: 1, loop: true }, { type: 1, x: 0.3, y: 300, speed_x: -14, speed_y: 1, loop: true }, { type: 1, x: 0.6, y: 600, speed_x: -14, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 1, y: 0, speed_x: 14, speed_y: 1, loop: true }, { type: 1, x: 0.5, y: 300, speed_x: 14, speed_y: 1, loop: true }, { type: 1, x: 0, y: 600, speed_x: 14, speed_y: 1, loop: true }]
                },
                {
                    datas: [{ type: 1, x: 0, y: 2000, speed_x: 12, speed_y: 2, loop: false }, { type: 1, x: -0.25, y: 400, speed_x: 12, speed_y: 2, loop: false }, { type: 1, x: -0.5, y: 600, speed_x: 12, speed_y: 2, loop: false }]
                }
            ];
            _this.missile_items = [
                [{ y: -200, speed_x: 5 }]
            ];
            _this.missile_items2 = [
                [{ y: -400, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }],
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }],
            ];
            _this.missile_items3 = [
                [{ y: -400, speed_x: 5 }],
                [{ y: -400, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }],
                [{ y: -400, speed_x: 5 }, { y: -500, speed_x: 5 }],
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }],
                [{ y: -400, speed_x: 5 }, { y: -500, speed_x: 5 }, { y: -600, speed_x: 5 }],
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }, { y: -600, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }, { y: -600, speed_x: 7 }]
            ];
            _this.missile_items4 = [
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }],
                [{ y: -400, speed_x: 8 }, { y: -500, speed_x: 8 }],
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }, { y: -600, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }, { y: -600, speed_x: 7 }],
                [{ y: -400, speed_x: 8 }, { y: -500, speed_x: 8 }, { y: -600, speed_x: 8 }],
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }, { y: -600, speed_x: 6 }, { y: -700, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }, { y: -600, speed_x: 7 }, { y: -700, speed_x: 7 }],
                [{ y: -400, speed_x: 8 }, { y: -500, speed_x: 8 }, { y: -600, speed_x: 8 }, { y: -700, speed_x: 8 }]
            ];
            _this.missile_items5 = [
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }],
                [{ y: -400, speed_x: 8 }, { y: -500, speed_x: 8 }],
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }, { y: -600, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }, { y: -600, speed_x: 7 }],
                [{ y: -400, speed_x: 8 }, { y: -500, speed_x: 8 }, { y: -600, speed_x: 8 }],
                [{ y: -400, speed_x: 6 }, { y: -500, speed_x: 6 }, { y: -600, speed_x: 6 }, { y: -700, speed_x: 6 }],
                [{ y: -400, speed_x: 7 }, { y: -500, speed_x: 7 }, { y: -600, speed_x: 7 }, { y: -700, speed_x: 7 }],
                [{ y: -400, speed_x: 8 }, { y: -500, speed_x: 8 }, { y: -600, speed_x: 8 }, { y: -700, speed_x: 8 }]
            ];
            _this.state = 0;
            _this.half_width = 0;
            _this.totalAnimScore = 0;
            _this.totalAnimAdd = 0;
            _this.honor = 0;
            _this.honor_next = 0;
            return _this;
        }
        //游戏运行
        battle.prototype.Run = function (has_tips) {
            game.starttime = Laya.timer.currTimer;
            this.half_width = game.width * 0.5;
            var scale = game.width / 640;
            //滚动背景
            this.back_scroll = new Scroll(this);
            this.airs = new Aircraft(this);
            //顶部
            this.content_top.pivot(this.content_top.width * 0.5, this.content_top.height * 0.5);
            this.content_top.pos(game.width * 0.5, 60);
            this.content_top.zOrder = 100;
            if (Laya.Browser.clientWidth < 640) {
                this.content_top.scale(scale, scale);
            }
            this.img1.visible = true;
            this.img2.visible = false;
            this.img3.visible = false;
            this.img4.visible = false;
            this.img5.visible = false;
            this.img6.visible = false;
            //======
            //警告
            this.alarm = new Laya.Sprite();
            this.alarm.loadImage('comp/alarm.png');
            this.alarm.scale(1, game.height / 960);
            this.alarm.zOrder = 100;
            this.addChild(this.alarm);
            this.timeline = new Laya.TimeLine;
            this.timeline.addLabel("a", 0).to(this.alarm, { alpha: 1 }, 250, null, 0)
                .addLabel("b", 0).to(this.alarm, { alpha: 0 }, 350, null, 0)
                .addLabel("c", 0).to(this.alarm, { alpha: 1 }, 450, null, 0)
                .addLabel("d", 0).to(this.alarm, { alpha: 0 }, 550, null, 0);
            this.alarm.visible = false;
            if (has_tips) {
                //提示
                var tips = new Laya.Sprite;
                tips.loadImage('comp/tips.png');
                tips.pivot(tips.width * 0.5, tips.height * 0.5);
                tips.pos(game.width * 0.5, game.height * 0.5);
                tips.zOrder = 1;
                if (Laya.Browser.clientWidth < 640) {
                    tips.scale(scale, scale);
                }
                this.addChild(tips);
                this.stage.on(Laya.Event.MOUSE_DOWN, this, this.CloseGuide, [tips]);
            }
            else {
                this.gameStar();
            }
        };
        battle.prototype.CloseGuide = function (img) {
            this.stage.offAll();
            img.destroy(true);
            this.gameStar();
        };
        battle.prototype.gameStar = function () {
            Laya.Tween.clearAll(this);
            this.totalScore = 0;
            this.distance = 0;
            this.alarm.visible = false;
            Laya.stage.on("mousedown", this, this.mouse_down);
            Laya.stage.on("mouseup", this, this.mouse_up);
            this.back_scroll.Start();
            this.airs.Start();
            this.player = new Player(this);
            this.SetValus();
            this.SetAirItems();
            this.SetMssileItems();
            this.state = 1;
        };
        battle.prototype.mouse_down = function () {
            if (this.state == 1) {
                this.state = -1;
                this.back_scroll.Run();
                this.player.Run();
            }
        };
        battle.prototype.mouse_up = function () {
            if (this.state == -1) {
                this.state = 1;
                this.back_scroll.Pause();
                this.player.Pause();
            }
        };
        battle.prototype.AddItem = function () {
        };
        battle.prototype.random = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        battle.prototype.SetScore = function (num) {
            this.totalScore = this.totalScore + num;
            if (num > 1) {
                this.totalAnimAdd = 1;
                Laya.timer.clear(this, this.ScoreAnim);
                Laya.timer.loop(10, this, this.ScoreAnim);
            }
            else {
                this.totalAnimScore = this.totalScore;
                this.text_score.text = this.totalAnimScore.toString();
            }
        };
        battle.prototype.SetScore2 = function (num) {
            this.totalScore = this.totalScore + num;
            this.totalAnimScore = this.totalScore;
            this.text_score.text = this.totalAnimScore.toString();
        };
        battle.prototype.ScoreAnim = function () {
            if (this.totalAnimScore < this.totalScore) {
                this.totalAnimScore = this.totalAnimScore + this.totalAnimAdd;
                if (this.totalAnimScore > this.totalScore) {
                    this.totalAnimScore = this.totalScore;
                    Laya.timer.clear(this, this.ScoreAnim);
                }
                this.text_score.text = this.totalAnimScore.toString();
            }
            else {
                Laya.timer.clear(this, this.ScoreAnim);
            }
        };
        battle.prototype.StopP = function (pp) {
            pp.stop();
        };
        //游戏结束
        battle.prototype.Over = function () {
            if (this.state != -100) {
                console.log('---game over---');
                this.state = -100;
                this.player.Over();
                this.back_scroll.Over();
                this.airs.Over();
                this.alarm.visible = true;
                this.alarm.alpha = 0;
                this.timeline.play(0, false);
                this.timeline.on(Laya.Event.COMPLETE, this, this.ShowEnd);
            }
        };
        battle.prototype.ShowEnd = function () {
            if (this.end == null) {
                this.end = new view.end();
                this.end.zOrder = 1;
                Laya.stage.addChild(this.end);
            }
        };
        battle.prototype.RelayOver = function () {
            console.log('-----RelayOver----');
            this.back_scroll.RealyOver();
            this.back_scroll = null;
            this.airs.RealyOver();
            this.airs = null;
            this.player.End();
            this.player = null;
        };
        battle.prototype.UpdateDis = function (num) {
            this.distance = num;
            this.text_distance.text = this.distance + "米";
            if (this.honor < 5) {
                if (this.distance >= this.honor_next) {
                    this.honor++;
                    this.SetValus();
                    this.player.PlayerEffect();
                }
            }
        };
        battle.prototype.SetValus = function () {
            switch (this.honor) {
                case 0:
                    this.honor_next = 100;
                    this.img1.visible = true;
                    this.back_scroll.speed = this.speeds[0];
                    this.back_scroll.missile_delay = this.missile_delays[0];
                    this.airs.delay = this.delays[0];
                    break;
                case 1:
                    this.honor_next = 200;
                    this.img1.visible = false;
                    this.img2.visible = true;
                    this.back_scroll.speed = this.speeds[1];
                    this.back_scroll.missile_delay = this.missile_delays[1];
                    this.airs.delay = this.delays[1];
                    break;
                case 2:
                    this.honor_next = 400;
                    this.img2.visible = false;
                    this.img3.visible = true;
                    this.back_scroll.speed = this.speeds[2];
                    this.back_scroll.missile_delay = this.missile_delays[2];
                    this.airs.delay = this.delays[2];
                    break;
                case 3:
                    this.honor_next = 600;
                    this.img3.visible = false;
                    this.img4.visible = true;
                    this.back_scroll.speed = this.speeds[3];
                    this.back_scroll.missile_delay = this.missile_delays[3];
                    this.airs.delay = this.delays[3];
                    break;
                case 4:
                    this.honor_next = 800;
                    this.img4.visible = false;
                    this.img5.visible = true;
                    this.back_scroll.speed = this.speeds[4];
                    this.back_scroll.missile_delay = this.missile_delays[4];
                    this.airs.delay = this.delays[4];
                    break;
                case 5:
                    this.img5.visible = false;
                    this.img6.visible = true;
                    this.back_scroll.speed = this.speeds[5];
                    this.back_scroll.missile_delay = this.missile_delays[5];
                    this.airs.delay = this.delays[5];
                    break;
            }
        };
        battle.prototype.SetMssileItems = function () {
            switch (this.honor) {
                case 0:
                    this.missile_items = this.missile_items2;
                    break;
                case 1:
                    this.missile_items = this.missile_items3;
                    break;
                case 2:
                    this.missile_items = this.missile_items4;
                    break;
                case 3:
                    this.missile_items = this.missile_items4;
                    break;
                case 4:
                    this.missile_items = this.missile_items5;
                    break;
                case 5:
                    this.missile_items = this.missile_items5;
                    break;
            }
        };
        battle.prototype.SetAirItems = function () {
            switch (this.honor) {
                case 0:
                    this.air_items = this.air_items2;
                    break;
                case 1:
                    this.air_items = this.air_items3;
                    break;
                case 2:
                    this.air_items = this.air_items4;
                    break;
                case 3:
                    this.air_items = this.air_items4;
                    break;
                case 4:
                    this.air_items = this.air_items5;
                    break;
                case 5:
                    this.air_items = this.air_items5;
                    break;
            }
        };
        return battle;
    }(ui.battleUI));
    view.battle = battle;
    //=======================
    var Scroll = /** @class */ (function () {
        function Scroll(_parent) {
            //设置数据
            this.speed = 7;
            this.missile_delay = 6000;
            //======
            this.items = new Array();
            this.count = 0;
            this.run = false;
            this.pick_up_min_dis = 20;
            this.pick_up_max_dis = 30;
            this.pick_up_next = 0;
            //==导弹
            this.missile_items = new Array();
            this.start_y = 0;
            this.move_y = 0;
            this.item_height = 1814;
            this.index_old = 0;
            this.index_now = 0;
            this.y = 0;
            this.parent = _parent;
            this.img = new Laya.Sprite();
            this.img.zOrder = 1;
            this.img.y = game.height - this.item_height;
            this.start_y = this.img.y;
            this.parent.addChild(this.img);
            var count = Math.ceil(game.height / this.item_height);
            for (var i = 0; i <= count; i++) {
                this.AddItem();
            }
        }
        Scroll.prototype.Start = function () {
            Laya.timer.frameLoop(1, this, this.Update);
            this.pick_up_next = -10;
            this.CreatePickUpItem();
            this.run = false;
            Laya.timer.once(1000, this, this.CreateMissile);
        };
        Scroll.prototype.Pause = function () {
            this.run = false;
        };
        Scroll.prototype.Run = function () {
            this.run = true;
        };
        Scroll.prototype.AddItem = function () {
            var item = new ScrollItem();
            item.Init(this.count * this.item_height * -1, this);
            this.items.push(item);
            this.count++;
        };
        Scroll.prototype.Update = function () {
            if (!this.run) {
                return;
            }
            this.img.y += this.speed;
            this.move_y = this.img.y - this.start_y;
            this.parent.UpdateDis(Math.floor(this.move_y / 100));
            this.index_now = Math.floor(this.move_y / this.item_height);
            if (this.index_old != this.index_now) {
                //console.log('--刷新:' + this.index_now);
                this.items[0].End();
                this.AddItem();
                this.index_old = this.index_now;
            }
            this.y = this.parent.player.y - this.img.y;
            if (this.y < this.pick_up_next) {
                this.pick_up_item.Over();
                this.CreatePickUpItem();
            }
        };
        Scroll.prototype.ReflashMissile = function () {
            for (var i = 0; i < this.missile_items.length; i++) {
                if (this.missile_items[i].state < 0) {
                    this.missile_items.splice(i, 1);
                    i--;
                }
            }
            if (this.missile_items.length == 0) {
                var d = this.parent.random(this.missile_delay * 1000 - 1000, this.missile_delay * 1000 + 1000);
                console.log('d:' + d + "----" + this.missile_delay);
                Laya.timer.once(d, this, this.CreateMissile);
            }
        };
        Scroll.prototype.CreateMissile = function () {
            console.log('----CreateMissile----');
            this.parent.SetMssileItems();
            var index = this.parent.random(0, this.parent.missile_items.length - 1);
            var objs = this.parent.missile_items[index];
            for (var i = 0; i < objs.length; i++) {
                console.log('CreateMissile-->' + i.toString());
                var obj = objs[i];
                var t = this.parent.random(0, 100) > 50 ? 1 : 2;
                var item = new MissileItem(this.y + obj.y, this, t, obj.speed_x);
                this.missile_items.push(item);
            }
        };
        Scroll.prototype.CreatePickUpItem = function () {
            var obj_index = this.parent.random(0, this.parent.items.length - 1);
            this.pick_up_next = this.parent.random(this.pick_up_min_dis * 100 - this.pick_up_next, this.pick_up_max_dis * 100 - this.pick_up_next) * -1;
            var item = new PickUpItem(this.pick_up_next, obj_index, this);
            this.pick_up_item = item;
        };
        Scroll.prototype.PickUpAnim = function (v) {
            var img = new Laya.Sprite();
            img.loadImage('comp/UI_8.png');
            img.pivot(img.width * 0.5, img.height);
            img.pos(game.width * 0.5, this.parent.player.y);
            img.scale(0.5, 0.5);
            img.zOrder = 20;
            var txt = new Laya.Label();
            txt.width = 50;
            txt.height = 30;
            txt.fontSize = 24;
            txt.color = "#ffffff";
            txt.align = 'left';
            txt.text = '+' + v;
            txt.pivot(0, txt.height * 0.5);
            img.addChild(txt);
            txt.pos(img.width, img.height * 0.5);
            this.parent.addChild(img);
            Laya.Tween.to(img, { y: img.y - 100 }, 800, Laya.Ease.linearIn, Laya.Handler.create(this, this.PickUpAnimOver, [img]));
            this.parent.SetScore(v);
        };
        Scroll.prototype.PickUpAnimOver = function (img) {
            img.destroy(true);
        };
        Scroll.prototype.RealyOver = function () {
            if (this.parent != null) {
                for (var i = 0; i < this.items.length; i++) {
                    this.items[i].End();
                }
                this.items = null;
                if (this.pick_up_item != null) {
                    this.pick_up_item.End();
                    this.pick_up_item = null;
                }
                for (var i = 0; i < this.missile_items.length; i++) {
                    this.missile_items[i].RealyOver(true);
                }
                this.missile_items = null;
                //===========
                this.parent = null;
                this.img.destroy(true);
            }
        };
        Scroll.prototype.Over = function () {
            Laya.timer.clearAll(this);
            for (var i = 0; i < this.missile_items.length; i++) {
                this.missile_items[i].Over();
            }
        };
        Scroll.prototype.GetTopY = function () {
            return this.img.y * -1;
        };
        return Scroll;
    }());
    var ScrollItem = /** @class */ (function () {
        function ScrollItem() {
        }
        ScrollItem.prototype.Init = function (y, parent) {
            this.bg = new Laya.Sprite();
            this.bg.loadImage('comp/game_bg_scroll.png');
            this.bg.pivot(this.bg.width * 0.5, 0);
            this.bg.pos(game.width * 0.5, y);
            this.bg.zOrder = 0;
            parent.img.addChild(this.bg);
            if (game.width < 640) {
                var scale = game.width / 640;
                this.bg.scale(scale, 1);
            }
        };
        ScrollItem.prototype.End = function () {
            this.bg.destroy();
        };
        return ScrollItem;
    }());
    var Aircraft = /** @class */ (function () {
        function Aircraft(_parent) {
            this.items = new Array();
            this.check_dis = 0;
            this.delay = 4000;
            this.parent = _parent;
        }
        Aircraft.prototype.Start = function () {
            Laya.timer.once(1000, this, this.CreateAir);
        };
        Aircraft.prototype.CreateAir = function () {
            this.parent.SetAirItems();
            var index = this.parent.random(0, this.parent.air_items.length - 1);
            var objs = this.parent.air_items[index].datas;
            for (var i = 0; i < objs.length; i++) {
                var air = new AircraftItem(this.parent.back_scroll.GetTopY() - objs[i].y, objs[i].x, this, objs[i].type, objs[i].speed_x, objs[i].speed_y, objs[i].loop);
                this.items.push(air);
            }
        };
        Aircraft.prototype.Reflash = function () {
            for (var i = 0; i < this.items.length; i++) {
                if (this.items[i].state < 0) {
                    this.items.splice(i, 1);
                    i--;
                }
            }
            if (this.items.length == 0) {
                Laya.timer.once(this.delay * 1000, this, this.CreateAir);
            }
        };
        Aircraft.prototype.RealyOver = function () {
            if (this.items != null) {
                for (var i = 0; i < this.items.length; i++) {
                    this.items[i].RealyOver();
                }
                this.items = null;
            }
        };
        Aircraft.prototype.Over = function () {
            Laya.timer.clearAll(this);
            if (this.items != null) {
                for (var i = 0; i < this.items.length; i++) {
                    this.items[i].Over();
                }
            }
        };
        return Aircraft;
    }());
    var AircraftItem = /** @class */ (function () {
        function AircraftItem(_y, _x, _parent, _type, _speed_x, _speed_y, _loop) {
            this.imgs = new Array();
            this.speed_x = 3;
            this.speed_y = 1.2;
            this.type = 0;
            this.x_min = 0;
            this.x_max = 0;
            this.state = 1;
            this.y = 0;
            this.angle = 0;
            this.start_y = 0;
            this.start_x = 0;
            this.width = 0;
            this.height = 0;
            this.img_index = 0;
            this.loop = false;
            this.y_max = 0;
            this.parent = _parent;
            this.type = _type;
            this.y = _y;
            this.speed_x = _speed_x;
            this.speed_y = _speed_y;
            this.loop = _loop;
            this.img = new Laya.Sprite();
            this.img.zOrder = 20;
            _parent.parent.addChild(this.img);
            for (var i = 1; i <= 2; i++) {
                var item_img = new Laya.Sprite();
                item_img.loadImage('comp/UI_' + i + '.png');
                item_img.pivot(item_img.width * 0.5, item_img.height * 0.5);
                item_img.visible = false;
                this.img.addChild(item_img);
                this.imgs.push(item_img);
            }
            this.width = this.imgs[0].width;
            this.height = this.imgs[0].height;
            this.imgs[0].visible = true;
            this.img_index = 0;
            var img_x = 0;
            if (_type == 1) { //左边的飞机
            }
            else if (_type == 3) {
                this.angle = 0;
                this.start_y = this.y;
            }
            else if (_type == 4) {
                this.angle = -90;
                this.start_y = this.y;
            }
            this.x_min = -200;
            this.x_max = game.width + 200;
            img_x = (this.x_max - this.x_min) * _x + this.x_min;
            this.start_x = img_x;
            this.img.pos(img_x, this.GetY(_y));
            Laya.timer.frameLoop(1, this, this.Update);
            Laya.timer.loop(50, this, this.AnimUpdate);
            this.y_max = game.height + this.height + 10;
        }
        AircraftItem.prototype.GetY = function (y) {
            return y + this.parent.parent.back_scroll.img.y;
        };
        AircraftItem.prototype.Update = function () {
            if (this.type == 1) { //直线飞行
                if (this.speed_x > 0 && this.img.x >= this.x_max) {
                    if (!this.loop) {
                        this.End();
                        return;
                    }
                    else {
                        this.speed_x = this.speed_x * -1;
                    }
                }
                else if (this.speed_x < 0 && this.img.x <= this.x_min) {
                    if (!this.loop) {
                        this.End();
                        return;
                    }
                    else {
                        this.speed_x = this.speed_x * -1;
                    }
                }
                this.img.x += this.speed_x;
                this.y += this.speed_y;
            }
            else if (this.type == 3) { //曲线
                if (this.speed_x > 0 && this.img.x >= this.x_max) {
                    if (!this.loop) {
                        this.End();
                        return;
                    }
                    else {
                        this.speed_x = this.speed_x * -1;
                    }
                }
                else if (this.speed_x < 0 && this.img.x <= this.x_min) {
                    if (!this.loop) {
                        this.End();
                        return;
                    }
                    else {
                        this.speed_x = this.speed_x * -1;
                    }
                }
                this.angle += 10;
                this.img.x += Math.cos(Math.PI / 180 * this.angle) + this.speed_x;
                this.start_y += this.speed_y;
                this.y = this.start_y + Math.sin(Math.PI / 180 * this.angle) * 50;
            }
            else if (this.type == 4) { //圆形
                this.angle += 2;
                this.img.x = this.start_x + Math.cos(Math.PI / 180 * this.angle) * this.speed_x;
                this.y = this.start_y + Math.sin(Math.PI / 180 * this.angle) * this.speed_y;
            }
            this.img.y = this.GetY(this.y);
            if (this.img.y > this.y_max) {
                this.End();
                return;
            }
            var dis_y = Math.abs(this.img.y - this.parent.parent.player.y);
            var dis_x = Math.abs(this.img.x - this.parent.parent.player.x);
            var min_x = this.width * 0.5 + this.parent.parent.player.width_half;
            var min_y = this.img.height * 0.5 + this.parent.parent.player.height_half;
            //console.log('dis_y:' + dis_y + ",dis_x:" + dis_x + ",min_x:" + min_x + ",min_y:" + min_y);
            if ((dis_x < min_x) && (dis_y < min_y)) {
                if ((this.parent.parent.player.state > 0) && (!this.parent.parent.player.revive)) {
                    //console.log("---over---" + this.parent.parent.player.state);
                    this.parent.parent.Over();
                    return;
                }
            }
        };
        AircraftItem.prototype.AnimUpdate = function () {
            this.imgs[this.img_index].visible = false;
            this.img_index++;
            if (this.img_index >= this.imgs.length) {
                this.img_index = 0;
            }
            this.imgs[this.img_index].visible = true;
        };
        AircraftItem.prototype.End = function () {
            if (this.state > 0) {
                this.state = -100;
                Laya.timer.clearAll(this);
                this.img.destroy(true);
                this.parent.Reflash();
                this.parent = null;
            }
        };
        AircraftItem.prototype.RealyOver = function () {
            if (this.state > 0) {
                this.state = -100;
                Laya.timer.clearAll(this);
                this.img.destroy(true);
                this.parent = null;
            }
        };
        AircraftItem.prototype.Over = function () {
            Laya.timer.clearAll(this);
        };
        return AircraftItem;
    }());
    var MissileItem = /** @class */ (function () {
        function MissileItem(_y, _parent, _type, _speed_x) {
            this.img_childs = new Array();
            this.img_index = 0;
            this.speed_x = 3;
            this.type = 0;
            this.x_min = 0;
            this.x_max = 0;
            this.state = 1;
            this.y = 0;
            this.parent = _parent;
            this.type = _type;
            this.speed_x = _speed_x;
            this.y = _y;
            this.img = new Laya.Sprite();
            this.img.zOrder = 20;
            this.img.width = 160;
            this.img.height = 74;
            this.img.pivot(this.img.width * 0.5, this.img.height * 0.5);
            _parent.parent.addChild(this.img);
            this.x_min = this.img.width * 0.5 * -1;
            this.x_max = game.width + this.img.width * 0.5;
            if (_type == 1) { //左边往右边飞
                this.img.pos(this.x_min, this.GetY(_y));
            }
            else { //右边往左边飞
                this.img.pos(this.x_max, this.GetY(_y));
                this.speed_x = this.speed_x * -1;
                this.img.scaleX = -1;
            }
            Laya.timer.frameLoop(1, this, this.Update);
            Laya.timer.loop(100, this, this.AnimUpdate);
            this.CreateIms('comp/UI_16.png');
            this.CreateIms('comp/UI_17.png');
            this.CreateIms('comp/UI_18.png');
            this.CreateIms('comp/UI_19.png');
            this.CreateIms('comp/UI_20.png');
            this.img_childs[0].visible = true;
        }
        MissileItem.prototype.CreateIms = function (name) {
            var img1 = new Laya.Sprite();
            img1.loadImage(name);
            img1.pivot(img1.width * 0.5, img1.height * 0.5);
            this.img.addChild(img1);
            this.img_childs.push(img1);
            img1.pos(0, 0);
            img1.visible = false;
        };
        MissileItem.prototype.AnimUpdate = function () {
            this.img_childs[this.img_index].visible = false;
            this.img_index++;
            if (this.img_index >= this.img_childs.length) {
                this.img_index = 0;
            }
            this.img_childs[this.img_index].visible = true;
        };
        MissileItem.prototype.Update = function () {
            if (this.type == 1) { //左边往右边飞
                if (this.img.x >= this.x_max) {
                    this.End(true);
                    return;
                }
                this.img.x += this.speed_x;
            }
            else if (this.type == 2) { //右边往左边飞
                if (this.img.x <= this.x_min) {
                    this.End(true);
                    return;
                }
                this.img.x += this.speed_x;
            }
            this.img.y = this.GetY(this.y);
            var dis_y = Math.abs(this.img.y - this.parent.parent.player.y);
            var dis_x = Math.abs(this.img.x - this.parent.parent.player.x);
            var min_x = this.img.width * 0.5 + this.parent.parent.player.width_half - 20;
            var min_y = this.img.height * 0.5 + this.parent.parent.player.height_half - 20;
            if (dis_x < min_x && dis_y < min_y) {
                if (this.parent.parent.player.state > 0 && !this.parent.parent.player.revive) {
                    var anim = new MissileAnim(this.parent.parent, this.img.x, this.img.y);
                    this.RealyOver(false);
                    this.parent.parent.Over();
                    return;
                }
            }
        };
        MissileItem.prototype.End = function (clear) {
            if (this.state > 0) {
                this.state = -100;
                Laya.timer.clearAll(this);
                if (this.img_childs != null) {
                    for (var i = 0; i < this.img_childs.length; i++) {
                        this.img_childs[i].destroy();
                    }
                    this.img_childs = null;
                }
                this.img.destroy(true);
                this.parent.ReflashMissile();
                if (clear) {
                    this.parent = null;
                }
            }
        };
        MissileItem.prototype.RealyOver = function (clear) {
            if (this.state > 0) {
                this.state = -100;
                Laya.timer.clearAll(this);
                if (this.img_childs != null) {
                    for (var i = 0; i < this.img_childs.length; i++) {
                        this.img_childs[i].destroy();
                    }
                    this.img_childs = null;
                }
                this.img.destroy(true);
                if (clear) {
                    this.parent = null;
                }
            }
        };
        MissileItem.prototype.GetY = function (y) {
            return y + this.parent.parent.back_scroll.img.y;
        };
        MissileItem.prototype.Over = function () {
            Laya.timer.clearAll(this);
        };
        return MissileItem;
    }());
    var MissileAnim = /** @class */ (function () {
        function MissileAnim(_parent, _x, _y) {
            this.imgs = new Array();
            this.parent = _parent;
            this.x = _x;
            this.y = _y;
            this.CreateIms('comp/UI_100.png');
            this.CreateIms('comp/UI_101.png');
            this.CreateIms('comp/UI_102.png');
            this.CreateIms('comp/UI_103.png');
            this.img_index = 0;
            Laya.timer.loop(100, this, this.AnimUpdate);
        }
        MissileAnim.prototype.CreateIms = function (name) {
            var img1 = new Laya.Sprite();
            img1.loadImage(name);
            img1.pivot(img1.width * 0.5, img1.height * 0.5);
            img1.zOrder = 100;
            this.parent.addChild(img1);
            this.imgs.push(img1);
            img1.pos(this.x, this.y);
            img1.visible = false;
        };
        MissileAnim.prototype.AnimUpdate = function () {
            this.imgs[this.img_index].visible = false;
            this.img_index++;
            if (this.img_index >= this.imgs.length) {
                this.Over();
                return;
            }
            this.imgs[this.img_index].visible = true;
        };
        MissileAnim.prototype.Over = function () {
            Laya.timer.clearAll(this);
            for (var i = 0; i < this.imgs.length; i++) {
                this.imgs[i].destroy(true);
            }
            this.imgs = null;
            this.parent = null;
        };
        return MissileAnim;
    }());
    var PickUpItem = /** @class */ (function () {
        function PickUpItem(_y, _obj_index, _parent) {
            this.y = _y;
            this.parent = _parent;
            var obj = _parent.parent.items[_obj_index];
            this.score = obj.score;
            this.img = new Laya.Sprite();
            this.img.loadImage('comp/' + obj.img + '.png');
            this.img.pivot(this.img.width * 0.5, this.img.height * 0.5);
            this.img.zOrder = 1;
            _parent.img.addChild(this.img);
            this.img.pos(game.width * 0.5, _y);
        }
        PickUpItem.prototype.Over = function () {
            this.parent.PickUpAnim(this.score);
            this.img.destroy(true);
        };
        PickUpItem.prototype.End = function () {
            this.img.destroy(true);
        };
        return PickUpItem;
    }());
    //==================
    var Player = /** @class */ (function () {
        function Player(_parent) {
            //人物动画图片
            this.imgs = new Array();
            this.effects = new Array();
            this.x = 0;
            this.y = 0;
            this.width_half = 0;
            this.height_half = 0;
            this.img_index = 1;
            this.state = 0;
            this.ReviveCD = 3000;
            this.ReviveAnimTime = 500;
            this.revive = false;
            this.effect_index = 0;
            this.parent = _parent;
            this.x = game.width * 0.5;
            this.y = game.height * 0.5 + 200;
            this.img = new Laya.Sprite();
            this.img.zOrder = 10;
            this.parent.addChild(this.img);
            this.img.pos(this.x, this.y);
            for (var i = 0; i < 12; i++) {
                var itme_img = new Laya.Sprite;
                if (i < 10) {
                    itme_img.loadImage("comp/0000" + i + ".png");
                }
                else {
                    itme_img.loadImage("comp/000" + i + ".png");
                }
                itme_img.pivot(itme_img.width * 0.5, itme_img.height * 0.5);
                this.img.addChild(itme_img);
                itme_img.visible = false;
                itme_img.x = 12;
                this.imgs.push(itme_img);
            }
            this.img_idle = new Laya.Sprite;
            this.img_idle.loadImage("comp/0.png");
            this.img_idle.pivot(this.img_idle.width * 0.5, this.img_idle.height * 0.5);
            this.img_idle.x = 12;
            this.img.addChild(this.img_idle);
            this.img_revive = new Laya.Sprite;
            this.img_revive.loadImage("comp/UI_14.png");
            this.img_revive.pivot(this.img_revive.width * 0.5, this.img_revive.height * 0.5);
            this.img_revive.y = -30;
            this.img_revive.x = 12;
            this.img.addChild(this.img_revive);
            this.img_revive.visible = false;
            this.img_cover = new Laya.Sprite;
            this.img_cover.loadImage("comp/UI_30.png");
            this.img_cover.pivot(this.img_cover.width * 0.5, 0);
            this.img_cover.pos(15, this.img_cover.height * -0.75);
            this.img.addChild(this.img_cover);
            this.img_cover.zOrder = 2;
            this.width_half = 40;
            this.height_half = 120;
            this.img_index = -1;
            this.Pause();
            Laya.timer.loop(80, this, this.UpdateImg);
            //===特效====
            for (var i = 1; i < 10; i++) {
                var effect_img = new Laya.Sprite();
                effect_img.loadImage('effect/UpgradeEffect_0' + i + '.png');
                effect_img.pivot(effect_img.width * 0.5, effect_img.height * 0.5);
                effect_img.scale(2.168, 2.168);
                effect_img.pos(0, 0);
                effect_img.zOrder = 3;
                this.img.addChild(effect_img);
                this.effects.push(effect_img);
                effect_img.visible = false;
            }
            for (var i = 10; i <= 12; i++) {
                var effect_img = new Laya.Sprite();
                effect_img.loadImage('effect/UpgradeEffect_' + i + '.png');
                effect_img.pivot(effect_img.width * 0.5, effect_img.height * 0.5);
                effect_img.scale(2.168, 2.168);
                effect_img.pos(0, 0);
                effect_img.zOrder = 3;
                this.img.addChild(effect_img);
                this.effects.push(effect_img);
                effect_img.visible = false;
            }
        }
        Player.prototype.UpdateImg = function () {
            if (this.state < 0) {
                return;
            }
            this.imgs[this.img_index].visible = false;
            this.img_index++;
            if (this.img_index > this.imgs.length - 1) {
                this.img_index = 0;
            }
            this.imgs[this.img_index].visible = true;
            if (this.revive) {
                this.imgs[this.img_index].alpha = 0.5;
            }
            else {
                this.imgs[this.img_index].alpha = 1;
            }
        };
        Player.prototype.Run = function () {
            this.state = 1;
            this.img_index = 0;
            this.imgs[this.img_index].visible = true;
            this.img_idle.visible = false;
            Laya.Tween.clearAll(this.img_cover);
            this.img_cover.visible = false;
        };
        Player.prototype.Pause = function () {
            this.state = -1;
            if (this.img_index >= 0) {
                this.imgs[this.img_index].visible = false;
            }
            this.img_idle.visible = true;
            this.img_cover.visible = true;
            this.img_cover.scale(1, 0);
            Laya.Tween.to(this.img_cover, { scaleY: 1 }, 200, Laya.Ease.linearIn, null, 0, true);
            this.img_idle.alpha = 0.5;
        };
        //复活
        Player.prototype.Revive = function () {
            if (this.state < 0) {
                this.img_idle.alpha = 0;
            }
            else {
                this.imgs[this.img_index].alpha = 0.5;
            }
            this.revive = true;
            this.img_revive.visible = true;
            this.Run();
            this.ReviveAnim1();
            Laya.timer.once(this.ReviveCD, this, this.ReviveOver);
        };
        Player.prototype.ReviveAnim1 = function () {
            Laya.Tween.to(this.img_revive, { scaleX: 0.8, scaleY: 0.8 }, 100, Laya.Ease.linearIn, Laya.Handler.create(this, this.ReviveAnim2), this.ReviveCD - this.ReviveAnimTime);
        };
        Player.prototype.ReviveAnim2 = function () {
            Laya.Tween.to(this.img_revive, { scaleX: 1, scaleY: 1 }, 100, Laya.Ease.linearIn, Laya.Handler.create(this, this.ReviveAnim3), 0);
        };
        Player.prototype.ReviveAnim3 = function () {
            Laya.Tween.to(this.img_revive, { scaleX: 0.8, scaleY: 0.8 }, 100, Laya.Ease.linearIn, Laya.Handler.create(this, this.ReviveAnim2), 0);
        };
        Player.prototype.PlayerEffect = function () {
            this.effect_index = 0;
            Laya.timer.loop(50, this, this.UpdateEffect);
        };
        Player.prototype.UpdateEffect = function () {
            this.effects[this.effect_index].visible = false;
            this.effect_index++;
            if (this.effect_index > 11) {
                this.effect_index = 0;
                Laya.timer.clear(this, this.UpdateEffect);
                return;
            }
            else {
                this.effects[this.effect_index].visible = true;
            }
        };
        Player.prototype.ReviveOver = function () {
            if (this.state < 0) {
                this.img_idle.alpha = 1;
            }
            Laya.Tween.clearAll(this.img_revive);
            this.img_revive.visible = false;
            this.revive = false;
        };
        Player.prototype.End = function () {
            this.parent = null;
            if (this.imgs != null) {
                for (var i = 0; i < this.imgs.length; i++) {
                    this.imgs[i].destroy(true);
                }
                this.imgs = null;
                for (var i = 0; i < this.effects.length; i++) {
                    this.effects[i].destroy(true);
                }
                this.effects = null;
                this.img_idle.destroy(true);
                this.img_revive.destroy(true);
                this.img_cover.destroy(true);
                this.img.destroy(true);
            }
        };
        Player.prototype.Over = function () {
            Laya.timer.clearAll(this);
            Laya.timer.clearAll(this.img);
        };
        return Player;
    }());
})(view || (view = {}));
//# sourceMappingURL=battle.js.map