var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var View = laya.ui.View;
var Dialog = laya.ui.Dialog;
var ui;
(function (ui) {
    var battleUI = /** @class */ (function (_super) {
        __extends(battleUI, _super);
        function battleUI() {
            return _super.call(this) || this;
        }
        battleUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.battleUI.uiView);
        };
        battleUI.uiView = { "type": "View", "props": { "width": 640, "height": 2048 }, "child": [{ "type": "Image", "props": { "var": "content_top", "skin": "comp/title_bg.png" }, "child": [{ "type": "Label", "props": { "y": 28, "x": 69, "width": 92, "var": "text_score", "text": "0", "height": 20, "fontSize": 20, "font": "宋体", "color": "#ffffff", "bold": true, "align": "left" } }, { "type": "Label", "props": { "y": 28, "x": 402, "width": 141, "var": "text_distance", "text": "0", "height": 20, "fontSize": 20, "font": "宋体", "color": "#ffffff", "bold": true, "align": "right" } }, { "type": "Image", "props": { "y": 17, "x": 218, "width": 124, "height": 40 }, "child": [{ "type": "Image", "props": { "var": "img1", "skin": "comp/UI_200.png" } }, { "type": "Image", "props": { "y": 0, "x": 0, "var": "img2", "skin": "comp/UI_201.png" } }, { "type": "Image", "props": { "y": 0, "x": 0, "var": "img3", "skin": "comp/UI_202.png" } }, { "type": "Image", "props": { "var": "img4", "skin": "comp/UI_203.png" } }, { "type": "Image", "props": { "var": "img5", "skin": "comp/UI_204.png" } }, { "type": "Image", "props": { "y": 0, "x": -24, "var": "img6", "skin": "comp/UI_205.png" } }] }] }] };
        return battleUI;
    }(View));
    ui.battleUI = battleUI;
})(ui || (ui = {}));
(function (ui) {
    var endUI = /** @class */ (function (_super) {
        __extends(endUI, _super);
        function endUI() {
            return _super.call(this) || this;
        }
        endUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.endUI.uiView);
        };
        endUI.uiView = { "type": "View", "props": { "width": 640, "height": 2048 }, "child": [{ "type": "Image", "props": { "y": -4, "x": 0, "width": 640, "var": "content", "height": 500 }, "child": [{ "type": "Label", "props": { "y": 0, "x": 0, "width": 640, "text": "成功逃离", "height": 34, "fontSize": 28, "font": "Arial", "color": "#ffffff", "align": "center" } }, { "type": "Label", "props": { "y": 39, "x": 0, "width": 640, "var": "txt_score", "text": "108米", "height": 83, "fontSize": 80, "font": "Arial", "color": "#ffffff", "align": "center" } }, { "type": "Label", "props": { "y": 190, "x": 0, "width": 640, "var": "txt_best", "text": "最佳:202米", "height": 50, "fontSize": 30, "font": "Arial", "color": "#ffffff", "align": "center" } }, { "type": "Button", "props": { "y": 328, "x": 232, "var": "btn_restart", "stateNum": 1, "skin": "comp/button_6.png", "labelStrokeColor": "#ffffff", "labelSize": 28, "labelFont": "Arial", "labelColors": "#ffffff", "labelBold": true, "labelAlign": "center", "label": "提交成绩" } }, { "type": "Button", "props": { "y": 340, "x": 148, "var": "btn_start", "stateNum": 1, "skin": "comp/button_5.png" } }, { "type": "Button", "props": { "y": 340, "x": 459, "var": "btn_restart2", "stateNum": 1, "skin": "comp/button_4.png" } }] }] };
        return endUI;
    }(View));
    ui.endUI = endUI;
})(ui || (ui = {}));
(function (ui) {
    var loginUI = /** @class */ (function (_super) {
        __extends(loginUI, _super);
        function loginUI() {
            return _super.call(this) || this;
        }
        loginUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.loginUI.uiView);
        };
        loginUI.uiView = { "type": "View", "props": { "width": 640, "height": 2048 } };
        return loginUI;
    }(View));
    ui.loginUI = loginUI;
})(ui || (ui = {}));
//# sourceMappingURL=layaUI.max.all.js.map