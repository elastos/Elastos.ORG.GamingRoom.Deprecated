// 程序入口
var GameMain = /** @class */ (function () {
    function GameMain() {
        //是否有声音
        this.has_audio = true;
        this.width = 640;
        this.height = 960;
        this.scale = 1;
    }
    //游戏开始
    GameMain.prototype.Run = function () {
        Laya.init(this.width, this.height, laya.webgl.WebGL);
        this.initStage();
        this.scale = 640 / Laya.Browser.clientWidth;
        this.height = this.scale * Laya.Browser.clientHeight;
        this.txt_loading = new Laya.Label();
        this.txt_loading.width = 300;
        this.txt_loading.height = 100;
        this.txt_loading.fontSize = 50;
        this.txt_loading.color = "#000000";
        this.txt_loading.align = 'center';
        this.txt_loading.text = '';
        this.txt_loading.pivot(this.txt_loading.width * 0.5, this.txt_loading.height * 0.5);
        this.txt_loading.pos(this.width * 0.5, this.height * 0.5);
        Laya.stage.addChild(this.txt_loading);
        this.uid = this.GetQueryString("uid");
        this.url = this.GetQueryString("serverurl");
        this.loadResource();
        //Laya.Stat.show(Laya.Browser.clientWidth - 120 >> 1, Laya.Browser.clientHeight - 100 >> 1);
    };
    //游戏暂停
    GameMain.prototype.GamePause = function () {
        if (game.bt != null) {
            //game.bt.GamePause();
        }
        else {
            console.error("game.bt为空");
        }
    };
    //游戏恢复
    GameMain.prototype.GameRestore = function (score) {
        if (game.bt != null) {
            //game.bt.GameRestore();
        }
        else {
            console.error("game.bt为空");
        }
    };
    //游戏结束
    GameMain.prototype.GameOver = function (score) {
        console.log("游戏结束：" + score);
    };
    //游戏复活
    GameMain.prototype.GameReStar = function () {
        if (game.bt != null) {
            game.bt.destroy();
        }
        if (game.login != null) {
            game.login.destroy();
        }
        this.Star();
    };
    //获取游戏分数
    GameMain.prototype.GetGameScore = function () {
        if (game.bt != null) {
            //return game.bt.totalScore;
        }
        return -1;
    };
    GameMain.prototype.initStage = function () {
        Laya.stage.alignV = Laya.Stage.ALIGN_CENTER;
        Laya.stage.scaleMode = "fixedwidth";
        Laya.stage.alignH = Laya.Stage.ALIGN_CENTER;
        Laya.stage.bgColor = "#ffffff";
    };
    GameMain.prototype.loadResource = function () {
        var uiResArry = [
            { url: "res/atlas/comp.atlas", type: Laya.Loader.ATLAS },
            { url: "res/atlas/effect.atlas", type: Laya.Loader.ATLAS },
            { url: "res/sound/lose.mp3", type: Laya.Loader.SOUND }
        ];
        Laya.loader.load(uiResArry, Laya.Handler.create(this, this.onLoadOk, null, false), Laya.Handler.create(this, this.onLoading, null, false));
    };
    GameMain.prototype.onLoadOk = function () {
        console.log("load over");
        if (this.txt_loading != null) {
            this.txt_loading.destroy(true);
        }
        this.Star();
    };
    GameMain.prototype.Star = function () {
        //testAutoPlay();
        this.bt = new view.battle();
        this.bt.zOrder = 1;
        Laya.stage.addChild(this.bt);
        this.login = new view.login();
        this.login.zOrder = 3;
        Laya.stage.addChild(this.login);
    };
    GameMain.prototype.ClearAll = function () {
        if (this.bt != null) {
            this.bt.destroy();
            this.bt = null;
        }
        if (this.login != null) {
            this.login.destroy();
            this.login = null;
        }
    };
    GameMain.prototype.RunBt = function (has_tip) {
        this.bt = new view.battle();
        this.bt.zOrder = 1;
        Laya.stage.addChild(this.bt);
        this.bt.Run(has_tip);
        if (game.has_audio) {
            Laya.SoundManager.stopAll();
            //Laya.SoundManager.playMusic("res/sound/bgm.mp3", 0);
        }
    };
    GameMain.prototype.onLoading = function (num) {
        this.txt_loading.text = "Loading " + Math.ceil(num * 100) + "%";
    };
    //获取url里面的参数
    GameMain.prototype.GetQueryString = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return r[2]; //注意这里不能用js里面的unescape方法
        return null;
    };
    GameMain.prototype.SetAudio = function (ok) {
        if (this.login != null) {
            this.login.CheckAudioBack(ok);
        }
    };
    return GameMain;
}());
var game = new GameMain();
game.Run(); //游戏开始接口
//# sourceMappingURL=GameMain.js.map